-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dld_ga
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Fruits et légumes'),(2,'Viandes et poissons'),(3,'Frais'),(4,'Epicerie salée');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `city_and_zip_code`
--

LOCK TABLES `city_and_zip_code` WRITE;
/*!40000 ALTER TABLE `city_and_zip_code` DISABLE KEYS */;
INSERT INTO `city_and_zip_code` VALUES (1,'Montrouge','92120'),(2,'Paris 14E ARR.','75014'),(3,'Paris 13E ARR.','75013'),(4,'Malakoff','92240'),(5,'Châtillon','92320'),(6,'Gentilly','94250'),(7,'Bagneux','92220');
/*!40000 ALTER TABLE `city_and_zip_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `credit_bundle`
--

LOCK TABLES `credit_bundle` WRITE;
/*!40000 ALTER TABLE `credit_bundle` DISABLE KEYS */;
INSERT INTO `credit_bundle` VALUES (1,'Unitaire',1,1),(2,'Cinq',5,4),(3,'Dizaine',10,7);
/*!40000 ALTER TABLE `credit_bundle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dispute_category`
--

LOCK TABLES `dispute_category` WRITE;
/*!40000 ALTER TABLE `dispute_category` DISABLE KEYS */;
INSERT INTO `dispute_category` VALUES (1,'Agressivité','Insultes, menaces, comportement véhément'),(2,'Don impropre à la consommation','Le don proposé était déjà périmé ou contaminé'),(3,'Don non conforme','Le don proposé ne correspondait pas à sa dénomination'),(4,'Quantité non conforme','Le don proposé n\'était pas en quantité décrite'),(5,'Don inexistant','Le don proposé n\'était pas présent au lieu de retrait'),(6,'Don  refusé','Refus de céder le don au moment du retrait'),(7,'Don non récupéré','Le bénéficiaire n\'est pas venu chercher le don');
/*!40000 ALTER TABLE `dispute_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `distance`
--

LOCK TABLES `distance` WRITE;
/*!40000 ALTER TABLE `distance` DISABLE KEYS */;
INSERT INTO `distance` VALUES (1,2,1.8),(1,3,3.8),(1,4,1.9),(1,5,2.9),(1,6,1.6),(1,7,2.8),(2,3,2.9),(2,4,3.1),(2,5,4.2),(2,6,3),(2,7,4.3),(3,4,5.2),(3,5,6.3),(3,6,2.2),(3,7,3.3),(4,5,2),(4,6,4),(4,7,3.3),(5,6,4.6),(5,7,2.2),(6,7,3.6);
/*!40000 ALTER TABLE `distance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `measures`
--

LOCK TABLES `measures` WRITE;
/*!40000 ALTER TABLE `measures` DISABLE KEYS */;
INSERT INTO `measures` VALUES (47,1),(48,1),(49,1),(1,3),(2,3),(3,3),(4,3),(6,3),(7,3),(8,3),(9,3),(10,3),(11,3),(12,3),(13,3),(14,3),(15,3),(16,3),(17,3),(25,3),(26,3),(27,3),(28,3),(30,3),(31,3),(32,3),(33,3),(34,3),(36,3),(37,3),(38,3),(39,3),(40,3),(56,3),(57,3),(58,3),(58,4),(1,5),(2,5),(3,5),(4,5),(5,5),(6,5),(9,5),(10,5),(11,5),(15,5),(16,5),(17,5),(18,5),(19,5),(25,5),(26,5),(27,5),(28,5),(29,5),(30,5),(31,5),(32,5),(33,5),(34,5),(35,5),(36,5),(37,5),(38,5),(39,5),(40,5),(42,5),(43,5),(45,5),(46,5),(56,5),(57,5),(58,5),(5,6),(7,6),(8,6),(11,6),(16,6),(17,6),(18,6),(19,6),(20,6),(21,6),(22,6),(23,6),(24,6),(25,6),(26,6),(27,6),(28,6),(29,6),(30,6),(31,6),(32,6),(33,6),(34,6),(35,6),(36,6),(37,6),(38,6),(39,6),(40,6),(41,6),(42,6),(43,6),(44,6),(45,6),(46,6),(50,6),(51,6),(52,6),(53,6),(54,6),(55,6),(56,6),(57,6),(41,7),(44,7),(47,7),(48,7),(49,7),(45,8),(46,8);
/*!40000 ALTER TABLE `measures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `preservation_method`
--

LOCK TABLES `preservation_method` WRITE;
/*!40000 ALTER TABLE `preservation_method` DISABLE KEYS */;
INSERT INTO `preservation_method` VALUES (1,'Température ambiante'),(2,'Réfrigération');
/*!40000 ALTER TABLE `preservation_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,1,1,'Ananas'),(2,1,1,'Kiwi'),(3,2,1,'Orange'),(4,2,1,'Citron jaune'),(5,3,2,'Fraise'),(6,4,1,'Potimarron'),(7,5,1,'Salade iceberg'),(8,5,1,'Salade feuille de chêne'),(9,6,1,'Pommes de terre charlotte'),(10,6,1,'Oignon blanc'),(11,6,1,'Oignon rouge'),(12,7,1,'Chou blanc'),(13,7,1,'Chou vert'),(14,7,1,'Chou rouge'),(15,8,1,'Topinambour'),(16,9,1,'Coeur de boeuf'),(17,9,1,'Tomate grappe'),(18,10,1,'Lentilles vertes'),(19,10,1,'Lentilles corail'),(20,12,1,'Abricots secs'),(21,12,1,'Raisins secs'),(22,13,1,'Amandes'),(23,13,1,'Pistaches'),(24,13,1,'Cerneaux de noix'),(25,14,2,'Steak haché'),(26,15,2,'Saucisse de toulouse'),(27,15,2,'Saucisse de porc aux herbes'),(28,16,2,'Côtelettes d\'agneau'),(29,17,2,'Gambas'),(30,18,2,'Filet de saumon'),(31,18,2,'Filet de truite'),(32,18,2,'Pavé de saumon'),(33,19,2,'Sole meunière'),(34,19,2,'Flétan'),(35,20,2,'Moules'),(36,21,2,'Pilons de poulet'),(37,21,2,'Blancs de poulet'),(38,22,2,'Filets de dinde'),(39,22,2,'Cuisses de dinde'),(40,23,2,'Caille'),(41,24,2,'Crème allégée'),(42,24,2,'Crème semi-épaisse'),(43,24,2,'Crème fraîche'),(44,24,2,'Crème fleurette'),(45,25,2,'Beurre doux'),(46,25,2,'Beurre salé'),(47,26,2,'Lait entier'),(48,26,2,'Lait demi-écrémé'),(49,26,2,'Lait facile à digérer'),(50,27,2,'Comté'),(51,27,2,'Gruyère râpé'),(52,28,2,'Chèvre'),(53,28,2,'Camembert'),(54,29,2,'Roquefort'),(55,29,2,'Fourme d\'ambert'),(56,30,2,'Danone activia'),(57,30,2,'Leader price'),(58,6,1,'Carottes');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product_origin`
--

LOCK TABLES `product_origin` WRITE;
/*!40000 ALTER TABLE `product_origin` DISABLE KEYS */;
INSERT INTO `product_origin` VALUES (1,'production personnelle'),(2,'achat'),(3,'achat bio');
/*!40000 ALTER TABLE `product_origin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `storage_accessibility`
--

LOCK TABLES `storage_accessibility` WRITE;
/*!40000 ALTER TABLE `storage_accessibility` DISABLE KEYS */;
INSERT INTO `storage_accessibility` VALUES (1,'autonome'),(2,'par demande au personnel');
/*!40000 ALTER TABLE `storage_accessibility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `storage_dimensions`
--

LOCK TABLES `storage_dimensions` WRITE;
/*!40000 ALTER TABLE `storage_dimensions` DISABLE KEYS */;
INSERT INTO `storage_dimensions` VALUES (1,'petit',30,30,30),(2,'moyen',50,50,50),(3,'grand',100,100,100);
/*!40000 ALTER TABLE `storage_dimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory` VALUES (1,1,'Fruits'),(2,1,'Légumes'),(3,1,'Fruits et légumes secs'),(4,2,'Boucherie'),(5,2,'Poissonerie'),(6,2,'Volaille'),(7,3,'Crèmerie'),(8,3,'Fromages'),(9,3,'Yaourts et desserts'),(10,3,'Plats préparés'),(11,4,'Pâtes, riz et semoule'),(12,4,'Huile et vinaigre'),(13,4,'Condiments et sauces'),(14,4,'Conserves'),(17,2,'Fruits de mer');
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subset`
--

LOCK TABLES `subset` WRITE;
/*!40000 ALTER TABLE `subset` DISABLE KEYS */;
INSERT INTO `subset` VALUES (1,1,'Fruits exotiques'),(2,1,'Agrumes'),(3,1,'Fruits rouges'),(4,2,'Courges et potirons'),(5,2,'Salades'),(6,2,'Carottes, pommes de terre, oignons'),(7,2,'Choux, poireaux, céleri'),(8,2,'Légumes anciens'),(9,2,'Tomates'),(10,3,'Lentilles '),(11,3,'Pois chiches'),(12,3,'Fruits secs'),(13,3,'Noix et graines'),(14,4,'Boeuf'),(15,4,'Porc'),(16,4,'Agneau'),(17,17,'Crevettes'),(18,5,'Saumon, thon, truite'),(19,5,'Autres poissons'),(20,17,'Coquillages'),(21,6,'Poulet'),(22,6,'Dinde'),(23,6,'Autres volailles'),(24,7,'Crème'),(25,7,'Beurre'),(26,7,'Lait'),(27,8,'Pâte dure'),(28,8,'Pâte molle'),(29,8,'Bleus'),(30,9,'Yaourt nature'),(31,9,'Yaourt brassé'),(32,9,'Yaourt aux fruits'),(33,9,'Crème dessert'),(34,10,'Végétarien'),(35,10,'Viande'),(36,10,'Poisson'),(37,11,'Pâtes complètes'),(38,11,'Pâtes de blé'),(39,11,'Riz blanc'),(40,11,'Riz complet'),(41,12,'Huile d\'olive'),(42,12,'Huile de colza'),(43,12,'Vinaigre balsamique'),(44,13,'Moutarde'),(45,13,'Pesto'),(46,13,'Sauce tomate'),(47,14,'Fruits au sirop'),(48,14,'Mélange de légumes');
/*!40000 ALTER TABLE `subset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` VALUES (1,'Bouteille(s)'),(2,'Boîte(s)'),(3,'Pièce(s)'),(4,'Botte(s)'),(5,'kg'),(6,'g'),(7,'L'),(8,'plaquette(s) (250g)');
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `weekday`
--

LOCK TABLES `weekday` WRITE;
/*!40000 ALTER TABLE `weekday` DISABLE KEYS */;
INSERT INTO `weekday` VALUES (1,'lundi'),(2,'mardi'),(3,'mercredi'),(4,'jeudi'),(5,'vendredi'),(6,'samedi'),(7,'dimanche');
/*!40000 ALTER TABLE `weekday` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-22 15:35:23
