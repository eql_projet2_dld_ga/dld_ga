/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  15/12/2021 16:06:13                      */
/*==============================================================*/


drop table if exists Category;

drop table if exists City_and_zip_code;

drop table if exists Credit_bundle;

drop table if exists Dispute;

drop table if exists Dispute_category;

drop table if exists Distance;

drop table if exists Donation;

drop table if exists Member;

drop table if exists Opening_hours;

drop table if exists Partner;

drop table if exists Preservation_method;

drop table if exists Product;

drop table if exists Product_origin;

drop table if exists Storage;

drop table if exists Storage_accessibility;

drop table if exists Storage_dimensions;

drop table if exists Storage_location;

drop table if exists Subcategory;

drop table if exists Subset;

drop table if exists Unavailability;

drop table if exists Unit;

drop table if exists Weekday;

drop table if exists credit_purchase;

drop table if exists measures;

/*==============================================================*/
/* Table : Category                                             */
/*==============================================================*/
create table Category
(
   id_category          int not null auto_increment,
   category_name        varchar(254),
   primary key (id_category)
);

/*==============================================================*/
/* Table : City_and_zip_code                                    */
/*==============================================================*/
create table City_and_zip_code
(
   id_city              int not null auto_increment,
   city                 varchar(254) not null,
   zip_code             varchar(254) not null,
   primary key (id_city)
);

/*==============================================================*/
/* Table : Credit_bundle                                        */
/*==============================================================*/
create table Credit_bundle
(
   id_bundle            int not null auto_increment,
   name                 varchar(254),
   credit_number        int not null,
   price                float,
   primary key (id_bundle)
);

/*==============================================================*/
/* Table : Dispute                                              */
/*==============================================================*/
create table Dispute
(
   id_dispute           int not null auto_increment,
   offender_partner_id  int,
   offender_member_id   int,
   complainant_member_id int,
   Donation_id          int not null,
   complainant_partner_id int,
   Dispute_category_id  int not null,
   dispute_date         datetime,
   open_description     varchar(254),
   handling_start_date  datetime,
   handling_end_date    datetime,
   recipient_refund_date datetime,
   donor_credit_cancellation_date datetime,
   primary key (id_dispute)
);

/*==============================================================*/
/* Table : Dispute_category                                     */
/*==============================================================*/
create table Dispute_category
(
   id_dispute_category  int not null auto_increment,
   category_name        varchar(254) not null,
   description          varchar(254),
   primary key (id_dispute_category)
);

/*==============================================================*/
/* Table : Distance                                             */
/*==============================================================*/
create table Distance
(
   Origin_id            int not null,
   Destination_id       int not null,
   avg_distance         float,
   primary key (Origin_id, Destination_id)
);

/*==============================================================*/
/* Table : Donation                                             */
/*==============================================================*/
create table Donation
(
   id_donation          int not null auto_increment,
   Unit_id              int not null,
   donor_id             int not null,
   Storage_id           int,
   recipient_id         int,
   City_and_zip_code_id int,
   Product_id           int not null,
   Product_origin_id    int not null,
   expiration_date      datetime,
   quantity             float,
   description          varchar(254),
   picture              varchar(254),
   street_name_and_number varchar(254),
   submission_date      datetime,
   booking_date         datetime,
   pick_up_date         datetime,
   booking_cancellation_date datetime,
   submission_cancellation_date datetime,
   primary key (id_donation)
);

/*==============================================================*/
/* Table : Member                                               */
/*==============================================================*/
create table Member
(
   id_member            int not null auto_increment,
   City_and_zip_code_id int not null,
   last_name            varchar(254),
   first_name           varchar(254),
   birth_date           datetime,
   street_name_and_number varchar(254),
   email                varchar(254),
   password             varchar(254),
   phone_number         varchar(254),
   picture              varchar(254),
   registration_date    datetime,
   validation_code      int,
   email_validation_date datetime,
   deregistration_date  datetime,
   primary key (id_member)
);

/*==============================================================*/
/* Table : Opening_hours                                        */
/*==============================================================*/
create table Opening_hours
(
   id_business_hours    int not null auto_increment,
   Weekday_id           int not null,
   Donation_id          int,
   Storage_location_id  int,
   Storage_id           int,
   opening_hour         int,
   opening_minute       int,
   closing_hour         int,
   closing_minute       int,
   break_start_hour     int,
   break_start_minute   int,
   break_end_hour       int,
   break_end_minute     int,
   primary key (id_business_hours)
);

/*==============================================================*/
/* Table : Partner                                              */
/*==============================================================*/
create table Partner
(
   id_partner           int not null auto_increment,
   City_and_zip_code_id int not null,
   appellation          varchar(254),
   email                varchar(254),
   password             varchar(254),
   registration_date    datetime,
   validation_code      int,
   email_validation_date datetime,
   admin_validation_date datetime,
   first_name           varchar(254),
   last_name            varchar(254),
   role                 varchar(254),
   birth_date           datetime,
   phone_number         varchar(254),
   deregistration_date  datetime,
   street_name_and_number varchar(254),
   primary key (id_partner)
);

/*==============================================================*/
/* Table : Preservation_method                                  */
/*==============================================================*/
create table Preservation_method
(
   id_preservation      int not null auto_increment,
   method               varchar(254),
   primary key (id_preservation)
);

/*==============================================================*/
/* Table : Product                                              */
/*==============================================================*/
create table Product
(
   id_product           int not null auto_increment,
   Subset_id            int not null,
   Preservation_method_id int not null,
   name                 varchar(254),
   primary key (id_product)
);

/*==============================================================*/
/* Table : Product_origin                                       */
/*==============================================================*/
create table Product_origin
(
   id_origin            int not null auto_increment,
   product_origin       varchar(254),
   primary key (id_origin)
);

/*==============================================================*/
/* Table : Storage                                              */
/*==============================================================*/
create table Storage
(
   id_storage           int not null auto_increment,
   Preservation_method_id int not null,
   Storage_location_id  int not null,
   Storage_dimensions_id int not null,
   Storage_accessibility_id int not null,
   name                 varchar(254),
   add_date             datetime,
   removal_date         datetime,
   primary key (id_storage)
);

/*==============================================================*/
/* Table : Storage_accessibility                                */
/*==============================================================*/
create table Storage_accessibility
(
   id_accessibility     int not null auto_increment,
   accessibility        varchar(254),
   primary key (id_accessibility)
);

/*==============================================================*/
/* Table : Storage_dimensions                                   */
/*==============================================================*/
create table Storage_dimensions
(
   id_dimensions        int not null auto_increment,
   size_category        varchar(254) not null,
   length               int,
   width                int,
   height               int,
   primary key (id_dimensions)
);

/*==============================================================*/
/* Table : Storage_location                                     */
/*==============================================================*/
create table Storage_location
(
   id_storage_location  int not null auto_increment,
   Partner_id           int not null,
   City_and_zip_code_id int,
   name                 varchar(254),
   phone_number         varchar(254),
   picture              varchar(254),
   street_name_and_number varchar(254),
   add_date             datetime,
   removal_date         datetime,
   primary key (id_storage_location)
);

/*==============================================================*/
/* Table : Subcategory                                          */
/*==============================================================*/
create table Subcategory
(
   id_subcategory       int not null auto_increment,
   Category_id          int not null,
   subcategory_name     varchar(254),
   primary key (id_subcategory)
);

/*==============================================================*/
/* Table : Subset                                               */
/*==============================================================*/
create table Subset
(
   id_subset            int not null auto_increment,
   Subcategory_id       int not null,
   subset_name          varchar(254),
   primary key (id_subset)
);

/*==============================================================*/
/* Table : Unavailability                                       */
/*==============================================================*/
create table Unavailability
(
   id_unavailability    int not null auto_increment,
   Storage_location_id  int,
   absent_member_id     int,
   start_date           datetime,
   end_date             datetime,
   declaration_date     datetime,
   cancellation_date    datetime,
   primary key (id_unavailability)
);

/*==============================================================*/
/* Table : Unit                                                 */
/*==============================================================*/
create table Unit
(
   id_unit              int not null auto_increment,
   name                 varchar(254) not null,
   primary key (id_unit)
);

/*==============================================================*/
/* Table : Weekday                                              */
/*==============================================================*/
create table Weekday
(
   id_weekday           int not null auto_increment,
   weekday_name         varchar(254) not null,
   primary key (id_weekday)
);

/*==============================================================*/
/* Table : credit_purchase                                      */
/*==============================================================*/
create table credit_purchase
(
   id_purchase          int not null auto_increment,
   buyer_id             int not null,
   bundle_id            int not null,
   purchase_date        datetime,
   quantity             int,
   primary key (id_purchase)
);

/*==============================================================*/
/* Table : measures                                             */
/*==============================================================*/
create table measures
(
   Product_id           int not null,
   Unit_id              int not null,
   primary key (Product_id, Unit_id)
);

alter table Dispute add constraint FK_adds_information foreign key (Dispute_category_id)
      references Dispute_category (id_dispute_category) on delete restrict on update restrict;

alter table Dispute add constraint FK_concerns_member foreign key (offender_member_id)
      references Member (id_member) on delete restrict on update restrict;

alter table Dispute add constraint FK_concerns_partner foreign key (offender_partner_id)
      references Partner (id_partner) on delete restrict on update restrict;

alter table Dispute add constraint FK_member_opens_dispute foreign key (complainant_member_id)
      references Member (id_member) on delete restrict on update restrict;

alter table Dispute add constraint FK_partner_opens_dispute foreign key (complainant_partner_id)
      references Partner (id_partner) on delete restrict on update restrict;

alter table Dispute add constraint FK_reports foreign key (Donation_id)
      references Donation (id_donation) on delete restrict on update restrict;

alter table Distance add constraint FK_Destination foreign key (Destination_id)
      references City_and_zip_code (id_city) on delete restrict on update restrict;

alter table Distance add constraint FK_Origin foreign key (Origin_id)
      references City_and_zip_code (id_city) on delete restrict on update restrict;

alter table Donation add constraint FK_chooses foreign key (Unit_id)
      references Unit (id_unit) on delete restrict on update restrict;

alter table Donation add constraint FK_donate foreign key (donor_id)
      references Member (id_member) on delete restrict on update restrict;

alter table Donation add constraint FK_has foreign key (Product_origin_id)
      references Product_origin (id_origin) on delete restrict on update restrict;

alter table Donation add constraint FK_is foreign key (Product_id)
      references Product (id_product) on delete restrict on update restrict;

alter table Donation add constraint FK_is_in foreign key (City_and_zip_code_id)
      references City_and_zip_code (id_city) on delete restrict on update restrict;

alter table Donation add constraint FK_receives foreign key (recipient_id)
      references Member (id_member) on delete restrict on update restrict;

alter table Donation add constraint FK_stocke foreign key (Storage_id)
      references Storage (id_storage) on delete restrict on update restrict;

alter table Member add constraint FK_lives_in foreign key (City_and_zip_code_id)
      references City_and_zip_code (id_city) on delete restrict on update restrict;

alter table Opening_hours add constraint FK_corresponds_to foreign key (Weekday_id)
      references Weekday (id_weekday) on delete restrict on update restrict;

alter table Opening_hours add constraint FK_is_available foreign key (Donation_id)
      references Donation (id_donation) on delete restrict on update restrict;

alter table Opening_hours add constraint FK_storage_opens foreign key (Storage_id)
      references Storage (id_storage) on delete restrict on update restrict;

alter table Opening_hours add constraint FK_storage_location_opens foreign key (Storage_location_id)
      references Storage_location (id_storage_location) on delete restrict on update restrict;

alter table Partner add constraint FK_is_located_in foreign key (City_and_zip_code_id)
      references City_and_zip_code (id_city) on delete restrict on update restrict;

alter table Product add constraint FK_compels foreign key (Preservation_method_id)
      references Preservation_method (id_preservation) on delete restrict on update restrict;

alter table Product add constraint FK_contains_denomination foreign key (Subset_id)
      references Subset (id_subset) on delete restrict on update restrict;

alter table Storage add constraint FK_describes foreign key (Preservation_method_id)
      references Preservation_method (id_preservation) on delete restrict on update restrict;

alter table Storage add constraint FK_is_composed_of foreign key (Storage_location_id)
      references Storage_location (id_storage_location) on delete restrict on update restrict;

alter table Storage add constraint FK_qualifies foreign key (Storage_accessibility_id)
      references Storage_accessibility (id_accessibility) on delete restrict on update restrict;

alter table Storage add constraint FK_sizes foreign key (Storage_dimensions_id)
      references Storage_dimensions (id_dimensions) on delete restrict on update restrict;

alter table Storage_location add constraint FK_is_precisely_in foreign key (City_and_zip_code_id)
      references City_and_zip_code (id_city) on delete restrict on update restrict;

alter table Storage_location add constraint FK_offers foreign key (Partner_id)
      references Partner (id_partner) on delete restrict on update restrict;

alter table Subcategory add constraint FK_contains_subcategory foreign key (Category_id)
      references Category (id_category) on delete restrict on update restrict;

alter table Subset add constraint FK_contains_subset foreign key (Subcategory_id)
      references Subcategory (id_subcategory) on delete restrict on update restrict;

alter table Unavailability add constraint FK_closes foreign key (Storage_location_id)
      references Storage_location (id_storage_location) on delete restrict on update restrict;

alter table Unavailability add constraint FK_leaves foreign key (absent_member_id)
      references Member (id_member) on delete restrict on update restrict;

alter table credit_purchase add constraint FK_bundle foreign key (bundle_id)
      references Credit_bundle (id_bundle) on delete restrict on update restrict;

alter table credit_purchase add constraint FK_buyer foreign key (buyer_id)
      references Member (id_member) on delete restrict on update restrict;

alter table measures add constraint FK_measures_denomination foreign key (Product_id)
      references Product (id_product) on delete restrict on update restrict;

alter table measures add constraint FK_measures_unit foreign key (Unit_id)
      references Unit (id_unit) on delete restrict on update restrict;

