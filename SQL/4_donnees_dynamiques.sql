-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dld_ga
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `credit_purchase`
--

LOCK TABLES `credit_purchase` WRITE;
/*!40000 ALTER TABLE `credit_purchase` DISABLE KEYS */;
INSERT INTO `credit_purchase` VALUES (1,3,1,'2021-12-15 11:04:05',2),(2,3,2,'2021-12-15 11:04:06',1);
/*!40000 ALTER TABLE `credit_purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dispute`
--

LOCK TABLES `dispute` WRITE;
/*!40000 ALTER TABLE `dispute` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `donation`
--

LOCK TABLES `donation` WRITE;
/*!40000 ALTER TABLE `donation` DISABLE KEYS */;
INSERT INTO `donation` VALUES (10,5,2,NULL,1,3,3,2,'2021-12-30 00:00:00',1,'Oranges navelles !','10.png','53 rue du Cheval','2021-12-18 16:31:46','2021-12-19 12:05:40',NULL,NULL,NULL),(11,4,4,NULL,NULL,3,58,1,'2022-01-05 00:00:00',2,'Du jardin, sans pesticides !','11.png','5 avenue Foch','2021-12-19 16:35:20',NULL,NULL,NULL,NULL),(12,3,1,11,NULL,NULL,4,3,'2021-12-28 00:00:00',6,'J\'ai eu une super promo, mais ça m\'en fait trop ...','12.png',NULL,'2021-12-20 16:43:22',NULL,NULL,NULL,NULL),(13,5,5,NULL,NULL,3,17,2,'2021-12-24 00:00:00',1,'Certaines commencent à s\'abîmer, dépêchez-vous','13.png','86 rue Drome','2021-12-21 16:46:37',NULL,NULL,NULL,NULL),(14,7,1,NULL,NULL,1,47,2,'2022-03-17 00:00:00',6,'Je me suis trompé en l\'achetant ...','14.png','1 rue de Paris','2021-12-21 16:52:06',NULL,NULL,NULL,NULL),(15,5,2,12,NULL,NULL,9,2,'2022-01-20 00:00:00',2,NULL,'15.png',NULL,'2021-12-21 16:57:10',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `donation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,1,'Malot','Jean','1990-03-25 00:00:00','1 rue de Paris','email@mail.com','123','0606060606','1.png','2021-12-14 12:40:00',858,'2021-12-14 12:45:00',NULL),(2,3,'Lepante','Willy','1987-05-20 00:00:00','53 rue du Cheval','lepante@mail.com','456','0707070707','2.png','2021-12-14 15:00:00',963,'2021-12-14 15:10:00',NULL),(3,6,'Kalanga','Babette','1952-12-05 00:00:00','182 rue Faure','babou@mail.fr','789','0607060706','3.png','2021-12-14 15:01:00',147,'2021-12-14 15:02:00',NULL),(4,3,'Tuale','Cécile','1978-02-19 00:00:00','5 avenue Foch','cectuale@mail.net','987','0607080607','4.png','2021-12-14 15:03:20',258,'2021-12-14 15:10:10',NULL),(5,4,'Denoël','Camille','1983-06-22 00:00:00','14bis allée Marchand','camille@mail.com','951','0705556689','5.png','2021-12-16 08:15:06',852,'2021-12-16 08:42:00',NULL),(6,5,'Balard','Adil','1972-04-19 00:00:00','103 boulevard Drouot','a.balard@mail.net','159','0698959293','6.png','2021-12-16 08:17:06',753,'2021-12-16 08:19:06','2021-12-16 09:15:06');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `opening_hours`
--

LOCK TABLES `opening_hours` WRITE;
/*!40000 ALTER TABLE `opening_hours` DISABLE KEYS */;
INSERT INTO `opening_hours` VALUES (1,1,NULL,1,NULL,8,0,18,30,NULL,NULL,NULL,NULL),(2,2,NULL,1,NULL,8,0,18,30,NULL,NULL,NULL,NULL),(3,4,NULL,1,NULL,8,0,18,30,NULL,NULL,NULL,NULL),(4,5,NULL,1,NULL,8,NULL,18,30,NULL,NULL,NULL,NULL),(5,6,NULL,1,NULL,9,NULL,18,0,NULL,NULL,NULL,NULL),(6,7,NULL,1,NULL,9,NULL,13,0,NULL,NULL,NULL,NULL),(7,1,NULL,2,NULL,10,30,20,30,14,15,15,NULL),(8,2,NULL,2,NULL,10,30,20,30,14,15,15,NULL),(9,3,NULL,2,NULL,8,NULL,18,NULL,NULL,NULL,NULL,NULL),(10,4,NULL,2,NULL,8,0,18,NULL,NULL,NULL,NULL,NULL),(11,6,NULL,2,NULL,8,NULL,18,NULL,NULL,NULL,NULL,NULL),(12,2,NULL,4,NULL,9,NULL,19,NULL,NULL,NULL,NULL,NULL),(13,3,NULL,4,NULL,9,NULL,19,NULL,12,NULL,13,NULL),(14,4,NULL,4,NULL,9,NULL,19,NULL,NULL,NULL,NULL,NULL),(15,1,10,NULL,NULL,16,0,20,0,0,0,0,0),(16,2,10,NULL,NULL,16,0,20,0,0,0,0,0),(17,3,10,NULL,NULL,16,0,20,0,0,0,0,0),(18,4,10,NULL,NULL,16,0,20,0,0,0,0,0),(19,5,10,NULL,NULL,16,0,20,0,0,0,0,0),(20,6,10,NULL,NULL,16,0,20,0,0,0,0,0),(21,1,11,NULL,NULL,10,30,14,0,0,0,0,0),(22,2,11,NULL,NULL,10,30,14,0,0,0,0,0),(23,3,11,NULL,NULL,10,30,14,0,0,0,0,0),(24,6,11,NULL,NULL,9,30,18,0,0,0,0,0),(25,7,11,NULL,NULL,9,30,18,0,0,0,0,0),(26,1,13,NULL,NULL,8,30,16,0,12,0,13,0),(27,2,13,NULL,NULL,8,30,16,0,12,0,13,0),(28,3,13,NULL,NULL,8,30,16,0,12,0,13,0),(29,4,13,NULL,NULL,8,30,16,0,12,0,13,0),(30,5,13,NULL,NULL,8,30,12,0,0,0,0,0),(31,1,14,NULL,NULL,18,0,20,0,0,0,0,0),(32,2,14,NULL,NULL,18,0,20,0,0,0,0,0),(33,3,14,NULL,NULL,18,0,20,0,0,0,0,0),(34,4,14,NULL,NULL,18,0,20,0,0,0,0,0),(35,5,14,NULL,NULL,18,0,20,0,0,0,0,0),(36,6,14,NULL,NULL,10,0,16,0,0,0,0,0);
/*!40000 ALTER TABLE `opening_hours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` VALUES (1,1,'Mairie de Montrouge','mairie@montrouge.fr','mairie','2021-12-12 15:00:00',123,'2021-12-13 03:00:00','2021-12-13 10:00:00','Etienne','Langereau','Maire','1950-01-14 00:00:00','0106050203',NULL,'1 place de la Mairie'),(2,1,'Boulangeries Martinot','contact@boulangem.com','pain','2021-12-12 15:10:00',456,'2021-12-12 15:10:01','2021-12-12 15:10:02','Corentin','Mafeux','CEO','1962-09-06 00:00:00','0636556496',NULL,'2 rue du Palais'),(3,2,'Maraîcher Bourdin','bourdin69@hotmail.fr','salade','2021-12-13 15:00:00',789,'2021-12-13 15:00:01','2021-12-13 15:00:02','Florinne','Bourdin','Gérante','1976-02-25 00:00:00','0165686963',NULL,'56 avenue de la république');
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `storage`
--

LOCK TABLES `storage` WRITE;
/*!40000 ALTER TABLE `storage` DISABLE KEYS */;
INSERT INTO `storage` VALUES (1,1,1,1,2,'Etagère 1','2021-12-16 00:00:00',NULL),(2,1,1,2,2,'Etagère 2','2021-12-16 00:00:00',NULL),(3,2,1,1,2,'Etagère frigo 1','2021-12-16 00:00:00',NULL),(4,1,2,3,2,'Etagère réserve 1','2021-12-16 00:00:00',NULL),(5,1,2,3,2,'Etagère réserve 2','2021-12-16 00:00:00',NULL),(6,1,3,2,1,'Box A','2021-12-17 00:00:00',NULL),(7,1,3,2,1,'Box B','2021-12-17 00:00:00',NULL),(8,1,3,2,1,'Box C','2021-12-17 00:00:00',NULL),(9,1,3,2,1,'Box D','2021-12-17 00:00:00',NULL),(10,2,4,1,2,'Frigo 1','2021-12-18 00:00:00',NULL),(11,1,4,1,2,'Etagère DLD 1','2021-12-18 00:00:00',NULL),(12,1,4,3,2,'Etagère DLD 2','2021-12-18 00:00:00',NULL);
/*!40000 ALTER TABLE `storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `storage_location`
--

LOCK TABLES `storage_location` WRITE;
/*!40000 ALTER TABLE `storage_location` DISABLE KEYS */;
INSERT INTO `storage_location` VALUES (1,2,1,'Boulangerie Martinot de l\'âne','0145896578','1.png','19 rue de l\'âne','2021-12-14 00:00:00',NULL),(2,2,1,'Boulangerie Martinot République','0156325874','2.png','53 avenue de la République','2021-12-14 00:00:00',NULL),(3,1,1,'Casier de box 1  Foch','','3.png','98 avenue Foch','2021-12-13 00:00:00',NULL),(4,3,2,'Maraîcher Bourdin','0147852369','4.png','135 avenue de la république','2021-12-13 16:00:02',NULL);
/*!40000 ALTER TABLE `storage_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `unavailability`
--

LOCK TABLES `unavailability` WRITE;
/*!40000 ALTER TABLE `unavailability` DISABLE KEYS */;
/*!40000 ALTER TABLE `unavailability` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-22 14:10:46
