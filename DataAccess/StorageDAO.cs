﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class StorageDAO : DAO
    {
        public Storage GetById(int storId)
        {
            Storage st = null;
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT
                                 Preservation_method_id,Storage_location_id,Storage_dimensions_id,
                                 Storage_accessibility_id,name,add_date,removal_date
                              FROM
                                 storage
                              WHERE
                                 id_storage = @id_storage";
            cmd.Parameters.Add(new MySqlParameter("id_storage", storId));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    st = new Storage();
                    st.StorId = dr.GetInt32(dr.GetOrdinal("id_storage"));
                    st.PreservationMethId = dr.GetInt32(dr.GetOrdinal("Preservation_method_id"));
                    st.StorLocationId = dr.GetInt32(dr.GetOrdinal("Storage_location_id"));
                    st.StorageDimensionsId = dr.GetInt32(dr.GetOrdinal("Storage_dimensions_id"));
                    st.StorageAccessibilityId = dr.GetInt32(dr.GetOrdinal("Storage_accessibility_id"));
                    st.Name = dr.GetString(dr.GetOrdinal("Name"));
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de creation d'un lieu de stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return st;

        }
        public List<Storage> GetAll()
        {
            List<Storage> sts = new List<Storage>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM storage";

            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Storage st = DataReaderToEntity(dr);
                sts.Add(st);
            }
            cnx.Close();

            return sts;
        }
        
        public void Insert(Storage st)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO storage
                              (Preservation_method_id,Storage_location_id,Storage_dimensions_id,
                                 Storage_accessibility_id,name,add_date,removal_date)
                              VALUES
                              (@Preservation_method_id,@Storage_location_id,@Storage_dimensions_id,
                               @Storage_accessibility_id,@name,@add_date,@removal_date)";
            cmd.Parameters.Add(new MySqlParameter("Preservation_method_id", st.PreservationMethId));
            cmd.Parameters.Add(new MySqlParameter("Storage_location_id", st.StorLocationId));
            cmd.Parameters.Add(new MySqlParameter("Storage_dimensions_id", st.StorageDimensionsId));
            cmd.Parameters.Add(new MySqlParameter("Storage_accessibility_id", st.StorageAccessibilityId));
            cmd.Parameters.Add(new MySqlParameter("name", st.Name));
            cmd.Parameters.Add(new MySqlParameter("add_date", st.AddDate));
            cmd.Parameters.Add(new MySqlParameter("removal_date", st.RemovalDate));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de creation d'un emplacement stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void Update(Storage st)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE storage
                               SET
                              Preservation_method_id=@Preservation_method_id,
                              Storage_location_id=@Storage_location_id,
                              Storage_dimensions_id=@Storage_dimensions_id,
                              Storage_accessibility_id=@Storage_accessibility_id,
                              name=@name,
                              add_date=@add_date,
                              removal_date=@removal_date
                              WHERE
                              id_storage=@id_storage";
            cmd.Parameters.Add(new MySqlParameter("id_storage", st.StorId));
            cmd.Parameters.Add(new MySqlParameter("Preservation_method_id", st.PreservationMethId));
            cmd.Parameters.Add(new MySqlParameter("Storage_location_id", st.StorLocationId));
            cmd.Parameters.Add(new MySqlParameter("Storage_dimensions_id", st.StorageDimensionsId));
            cmd.Parameters.Add(new MySqlParameter("Storage_accessibility_id", st.StorageAccessibilityId));
            cmd.Parameters.Add(new MySqlParameter("name", st.Name));
            cmd.Parameters.Add(new MySqlParameter("add_date", st.AddDate));
            cmd.Parameters.Add(new MySqlParameter("removal_date", st.RemovalDate));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de modification d'un emplacement stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void Desactive(Storage st)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE storage
                               SET
                              removal_date=@removal_date 
                              WHERE
                              id_storage=@id_storage";
            cmd.Parameters.Add(new MySqlParameter("removal_date", st.RemovalDate));
            cmd.Parameters.Add(new MySqlParameter("id_storage", st.StorId));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de désactiviation d'un emplacement stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public List<StorageDetails> GetByMultiCriteria(int StorLocationId)
        {
            List<StorageDetails> sds = new List<StorageDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT s.*, l.name 'location_name', p.method 'method_name', 
                              a.accessibility 'acces_name', d.size_category 'size_name'
                                FROM storage s
                                LEFT JOIN storage_location l
                                    ON s.Storage_location_id = l.id_storage_location
                                LEFT JOIN preservation_method p
                                    ON s.Preservation_method_id = p.id_preservation
                                LEFT JOIN Storage_accessibility a
                                    ON s.Storage_accessibility_id = a.id_accessibility
                                LEFT JOIN Storage_dimensions d
                                    ON s.Storage_dimensions_id = d.id_dimensions
                                WHERE (s.removal_date IS NULL)
                                AND (Storage_location_id = @StorLocationId OR @StorLocationId = 0)                                                  
                              ";
            cmd.Parameters.Add(new MySqlParameter("StorLocationId", StorLocationId));
 //           cmd.Parameters.Add(new MySqlParameter("PreservationMethId", PreservationMethId));
 //           cmd.Parameters.Add(new MySqlParameter("StorageDimensionsId", StorageDimensionsId));
 //           cmd.Parameters.Add(new MySqlParameter("StorageAccessibilityId", StorageAccessibilityId));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Storage st = DataReaderToEntity(dr);

                    StorageDetails storageDetails = new StorageDetails(st);
                    if (!dr.IsDBNull(dr.GetOrdinal("location_name")))
                        storageDetails.LocationName = dr.GetString(dr.GetOrdinal("location_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("method_name")))
                        storageDetails.MethodName = dr.GetString(dr.GetOrdinal("method_name"));                   
                    if (!dr.IsDBNull(dr.GetOrdinal("acces_name")))
                        storageDetails.AccesName = dr.GetString(dr.GetOrdinal("acces_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("size_name")))
                        storageDetails.DimensionName = dr.GetString(dr.GetOrdinal("size_name"));
                    sds.Add(storageDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(
                    "Erreur lors de la sélection d'un emplacement stockage : "
                    + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return sds;
        }
        #region READ
        public List<StorageDetails> GetAllAvailableByPreservationMethod(int id)
        {
            List<StorageDetails> results = new List<StorageDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
                                                                            // , sd.length, sd.width, sd.height
            cmd.CommandText = @"SELECT s.*, sl.name locname, sl.street_name_and_number, cz.city, cz.zip_code, 
                                pm.method, sa.accessibility, sd.size_category

                                FROM storage s	
                                JOIN storage_location sl            ON s.Storage_location_id = sl.id_storage_location
                                JOIN city_and_zip_code cz           ON sl.city_and_zip_code_id = cz.id_city
                                JOIN preservation_method pm         ON s.preservation_method_id = pm.id_preservation
                                JOIN storage_accessibility sa       ON s.Storage_accessibility_id = sa.id_accessibility
                                JOIN storage_dimensions sd          ON s.Storage_dimensions_id = sd.id_dimensions

                                LEFT JOIN 	(SELECT d.* 
			                                FROM donation d
			                                WHERE	
				                                d.Storage_id IS NOT NULL
				                                AND d.pick_up_date IS NULL 
				                                AND d.expiration_date > sysdate() 
				                                AND d.submission_cancellation_date IS NULL
				                                AND d.booking_cancellation_date IS NULL) AS d

		                                                            ON d.storage_id = s.id_storage
                                WHERE 
	                                d.storage_id IS NULL
                                    AND s.preservation_method_id = @preservId
                                
                                ORDER BY cz.zip_code, locname, s.name
                                ";
            cmd.Parameters.Add(new MySqlParameter("preservId", id));
            
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Storage storage = DataReaderToEntity(dr);

                    StorageDetails storageDetails = new StorageDetails(storage);

                    storageDetails.Accessibility = dr.GetString(dr.GetOrdinal("accessibility"));
                    storageDetails.Dimensions = dr.GetString(dr.GetOrdinal("size_category"));
                    storageDetails.StoragePreservationMethod = dr.GetString(dr.GetOrdinal("method"));
                    storageDetails.StorageLocationName = dr.GetString(dr.GetOrdinal("locname"));
                    storageDetails.StreetNameNumber = dr.GetString(dr.GetOrdinal("street_name_and_number"));
                    storageDetails.City = dr.GetString(dr.GetOrdinal("city"));
                    storageDetails.ZipCode = dr.GetString(dr.GetOrdinal("zip_code"));
                   
                    results.Add(storageDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des stockages disponibles : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return results;
        }
        #endregion

        #region PRIVATE
        private Storage DataReaderToEntity(DbDataReader dr)
        {
            Storage storage = new Storage();

            storage.StorId = dr.GetInt32(dr.GetOrdinal("id_storage"));
            storage.PreservationMethId = dr.GetInt32(dr.GetOrdinal("preservation_method_id"));
            storage.StorLocationId = dr.GetInt32(dr.GetOrdinal("storage_location_id"));
            storage.StorageDimensionsId = dr.GetInt32(dr.GetOrdinal("storage_dimensions_id"));
            storage.StorageAccessibilityId = dr.GetInt32(dr.GetOrdinal("storage_accessibility_id"));
            storage.Name = dr.GetString(dr.GetOrdinal("name"));
            storage.AddDate = dr.GetDateTime(dr.GetOrdinal("add_date"));
            if (!dr.IsDBNull(dr.GetOrdinal("removal_date")))
                storage.RemovalDate = dr.GetDateTime(dr.GetOrdinal("removal_date"));
          
            return storage;
        }
        #endregion
    }
}
