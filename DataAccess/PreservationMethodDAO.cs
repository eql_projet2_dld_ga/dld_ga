﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class PreservationMethodDAO : DAO
    {
        public List<PreservationMethod> GetAll()
        {
            List<PreservationMethod> pms = new List<PreservationMethod>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM preservation_method";
            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                PreservationMethod pm = DataReaderToEntity(dr);
                pms.Add(pm);
            }
            cnx.Close();
            return pms;
        }
        private PreservationMethod DataReaderToEntity(DbDataReader dr)
        {
            PreservationMethod pm = new PreservationMethod();
            pm.Id = dr.GetInt32(dr.GetOrdinal("id_preservation"));
            if (!dr.IsDBNull(dr.GetOrdinal("method")))
            {
                pm.Method = dr.GetString(dr.GetOrdinal("method"));
            }
            return pm;
        }
        public PreservationMethod GetById(int id)
        {
            PreservationMethod result = new PreservationMethod();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT *
                                FROM preservation_method
                                WHERE id_preservation = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result.Id = dr.GetInt32(dr.GetOrdinal("id_preservation"));
                    result.Method = dr.GetString(dr.GetOrdinal("method"));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de la récupération du produit : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
    }
}
