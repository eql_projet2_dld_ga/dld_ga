﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class SubCategoryDAO : DAO
    {
        #region READ
        public List<SubCategory> GetByCategoryId(int id)
        {
            List<SubCategory> results = new List<SubCategory>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT *
                                FROM subcategory
                                WHERE category_id = @category_id
                                ORDER BY subcategory_name";

            cmd.Parameters.Add(new MySqlParameter("category_id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    SubCategory subcat = new SubCategory();
                    subcat.Id = dr.GetInt32(dr.GetOrdinal("id_subcategory"));
                    subcat.CategoryId = dr.GetInt32(dr.GetOrdinal("category_id"));
                    subcat.Name = dr.GetString(dr.GetOrdinal("subcategory_name"));

                    results.Add(subcat);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de la récupération des subcategories de la category : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return results;
        }
        #endregion
    }
}
