﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class CreditPurchaseDAO : DAO
    {
        #region READ
        public List<CreditPurchaseDetails> GetAllDetails()
        {
            List<CreditPurchaseDetails> results = new List<CreditPurchaseDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT cp.*, m.first_name 'donor_first_name', m.last_name 'donor_last_name', 
                                        cb.name 'bundlename', cb.credit_number 'bundle_credit_number', cb.price 'bundle_price'
                                FROM credit_purchase cp
                                    LEFT JOIN credit_bundle cb ON cp.bundle_id = cb.id_bundle
                                    LEFT JOIN member m ON cp.buyer_id = m.id_member
                                ORDER BY m.id_member, cp.purchase_date";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    CreditPurchaseDetails purchase = new CreditPurchaseDetails();

                    purchase.PurchaseId = dr.GetInt32(dr.GetOrdinal("id_purchase"));
                    purchase.BundleId = dr.GetInt32(dr.GetOrdinal("bundle_id"));
                    purchase.BuyerId = dr.GetInt32(dr.GetOrdinal("buyer_id"));
                    purchase.Quantity = dr.GetInt32(dr.GetOrdinal("quantity"));
                    purchase.PurchaseDate = dr.GetDateTime(dr.GetOrdinal("purchase_date"));

                    purchase.BundleCreditNumber = dr.GetInt32(dr.GetOrdinal("bundle_credit_number"));
                    purchase.BundlePrice = dr.GetFloat(dr.GetOrdinal("bundle_price"));
                    purchase.MemberLastName = dr.GetString(dr.GetOrdinal("donor_last_name"));
                    purchase.MemberFirstName = dr.GetString(dr.GetOrdinal("donor_first_name"));
                    purchase.BundleName = dr.GetString(dr.GetOrdinal("bundlename"));

                    results.Add(purchase);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des achats : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return results;
        }
        #endregion
    }
}
