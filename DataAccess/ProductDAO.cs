﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class ProductDAO : DAO
    {
        #region READ
        public Product GetById(int id)
        {
            Product result = new Product();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT *
                                FROM product
                                WHERE id_product = @id
                                ORDER BY name";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result.Id = dr.GetInt32(dr.GetOrdinal("id_product"));
                    result.SubsetId = dr.GetInt32(dr.GetOrdinal("subset_id"));
                    result.PreservationId = dr.GetInt32(dr.GetOrdinal("preservation_method_id"));
                    result.Name = dr.GetString(dr.GetOrdinal("name"));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de la récupération du produit : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        public List<Product> GetBySubsetId(int id)
        {
            List<Product> results = new List<Product>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT *
                                FROM product
                                WHERE subset_id = @subset_id
                                ORDER BY name";

            cmd.Parameters.Add(new MySqlParameter("subset_id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Product product = new Product();
                    product.Id = dr.GetInt32(dr.GetOrdinal("id_product"));
                    product.SubsetId = dr.GetInt32(dr.GetOrdinal("subset_id"));
                    product.PreservationId = dr.GetInt32(dr.GetOrdinal("preservation_method_id"));
                    product.Name = dr.GetString(dr.GetOrdinal("name"));

                    results.Add(product);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de la récupération des produits du subset : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return results;
        }
        #endregion
    }
}
