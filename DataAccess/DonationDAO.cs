﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class DonationDAO : DAO
    {
        #region CREATE
        public void Insert(Donation don)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);

            DbCommand cmd = cnx.CreateCommand();

            if (don.CityAndZipCodeId > 0)
            {
                cmd.CommandText = @"INSERT INTO donation
	                                (unit_id, donor_id, product_id, product_origin_id, expiration_date, quantity, description, picture, submission_date, city_and_zip_code_id, street_name_and_number)
                                VALUES 
                                    (@unitId, @donorId, @productId, @originId, @expirationDate, @quantity, @description, @picture, @submissionDate, @cityZipCodeId, @streetNameNumber)";
  
                cmd.Parameters.Add(new MySqlParameter("cityZipCodeId", don.CityAndZipCodeId));
                cmd.Parameters.Add(new MySqlParameter("streetNameNumber", don.StreetNameNumber));
            }
            else if (don.StorageId > 0)
            {
                cmd.CommandText = @"INSERT INTO donation
	                                (unit_id, donor_id, product_id, product_origin_id, expiration_date, quantity, description, picture, submission_date, storage_id)
                                VALUES 
                                    (@unitId, @donorId, @productId, @originId, @expirationDate, @quantity, @description, @picture, @submissionDate, @storageId)";

                cmd.Parameters.Add(new MySqlParameter("storageId", don.StorageId));
            }
            else
            {
                cmd.CommandText = @"INSERT INTO donation
	                                (unit_id, donor_id, product_id, product_origin_id, expiration_date, quantity, description, picture, submission_date)
                                VALUES 
                                    (@unitId, @donorId, @productId, @originId, @expirationDate, @quantity, @description, @picture, @submissionDate)";
            }

                cmd.Parameters.Add(new MySqlParameter("unitId", don.UnitId));
                cmd.Parameters.Add(new MySqlParameter("donorId", don.DonorId));
                cmd.Parameters.Add(new MySqlParameter("productId", don.ProductId));
                cmd.Parameters.Add(new MySqlParameter("originId", don.ProductOriginId));
                cmd.Parameters.Add(new MySqlParameter("expirationDate", don.ExpirationDate));
                cmd.Parameters.Add(new MySqlParameter("quantity", don.Quantity));
                cmd.Parameters.Add(new MySqlParameter("description", don.Description));
                cmd.Parameters.Add(new MySqlParameter("picture", don.Picture));
                cmd.Parameters.Add(new MySqlParameter("submissionDate", DateTime.Now));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'insertion d'un nouveau don : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void BookDonation(DonationDetails donDetails)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"UPDATE donation
	                                  SET booking_date = @BookingDate,
                                          recipient_id = @RecipientId
                                    WHERE 
                                        id_donation = @DonationId";
            cmd.Parameters.Add(new MySqlParameter("BookingDate", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("RecipientId", donDetails.RecipientId)); 
            cmd.Parameters.Add(new MySqlParameter("DonationId", donDetails.Id));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la reservation d'un don : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void CancelDonation(DonationDetails donDetails)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"UPDATE donation
	                                  SET submission_cancellation_date = @SubmissionCancellationDate
                                    WHERE 
                                        id_donation = @DonationId";

            cmd.Parameters.Add(new MySqlParameter("SubmissionCancellationDate", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("DonationId", donDetails.Id));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la suppression d'un don : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void UpdatePicture(string pictureName, int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"UPDATE donation
	                            SET picture = @picture
                                WHERE id_donation = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            cmd.Parameters.Add(new MySqlParameter("picture", pictureName));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la mise à jour de l'image du don : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

        }

        public int InsertId(Donation don)
        {
            int result = 0;
            DbConnection cnx = new MySqlConnection(CNX_STRING);

            DbCommand cmd = cnx.CreateCommand();

            if (don.CityAndZipCodeId > 0)
            {
                cmd.CommandText = @"INSERT INTO donation
	                                (unit_id, donor_id, product_id, product_origin_id, expiration_date, quantity, 
                                    description, picture, submission_date, city_and_zip_code_id, street_name_and_number)
                                VALUES 
                                    (@unitId, @donorId, @productId, @originId, @expirationDate, @quantity, 
                                     @description, @picture, @submissionDate, @cityZipCodeId, @streetNameNumber)
                                ;
                                SELECT LAST_INSERT_ID()";
  
                cmd.Parameters.Add(new MySqlParameter("cityZipCodeId", don.CityAndZipCodeId));
                cmd.Parameters.Add(new MySqlParameter("streetNameNumber", don.StreetNameNumber));
            }
            else if (don.StorageId > 0)
            {
                cmd.CommandText = @"INSERT INTO donation
	                                (unit_id, donor_id, product_id, product_origin_id, expiration_date, quantity, 
                                    description, picture, submission_date, storage_id)
                                VALUES 
                                    (@unitId, @donorId, @productId, @originId, @expirationDate, @quantity, 
                                    @description, @picture, @submissionDate, @storageId)
                                ;
                                SELECT LAST_INSERT_ID()";

                cmd.Parameters.Add(new MySqlParameter("storageId", don.StorageId));
            }
            else
            {
                cmd.CommandText = @"INSERT INTO donation
	                                (unit_id, donor_id, product_id, product_origin_id, expiration_date, 
                                     quantity, description, picture, submission_date)
                                VALUES 
                                    (@unitId, @donorId, @productId, @originId, @expirationDate, 
                                    @quantity, @description, @picture, @submissionDate)
                                ;
                                SELECT LAST_INSERT_ID()";
            }

                cmd.Parameters.Add(new MySqlParameter("unitId", don.UnitId));
                cmd.Parameters.Add(new MySqlParameter("donorId", don.DonorId));
                cmd.Parameters.Add(new MySqlParameter("productId", don.ProductId));
                cmd.Parameters.Add(new MySqlParameter("originId", don.ProductOriginId));
                cmd.Parameters.Add(new MySqlParameter("expirationDate", don.ExpirationDate));
                cmd.Parameters.Add(new MySqlParameter("quantity", don.Quantity));
                cmd.Parameters.Add(new MySqlParameter("description", don.Description));
                cmd.Parameters.Add(new MySqlParameter("picture", don.Picture));
                cmd.Parameters.Add(new MySqlParameter("submissionDate", DateTime.Now));

            try
            {
                cnx.Open();
                result = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de l'insertion d'un nouveau don : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        #endregion

        #region READ
        public List<DonationDetails> GetByMultiCriteria(int ProductOriginId, string City, int ZipCode, int StorageId, int ProductId)
        {
            List<DonationDetails> result = new List<DonationDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT d.* ,u.name 'unit_name',czd.city 'city_name_donation', czd.zip_code 'zip_code_donation',
                                            po.product_origin 'product_origin_name', p.name 'product_name',
                                            s.name 'storage_name',md.last_name 'recipient_last_name', md.first_name 'recipient_first_name', sl.name 'storage_location_name',
                                            sl.street_name_and_number 'storage_location_address', czs.city 'city_name_storage', czs.zip_code 'zip_code_storage'
	                            FROM donation d
	                            LEFT JOIN unit u 
		                            ON d.Unit_id = u.id_unit 
	                            LEFT JOIN member md 
		                            ON d.recipient_id = md.id_member
	                            LEFT JOIN city_and_zip_code czd 
		                            ON d.City_and_zip_code_id = czd.id_city
	                            LEFT JOIN product_origin po 
		                            ON d.Product_origin_id = po.id_origin
	                            LEFT JOIN product p 
		                            ON d.Product_id = p.id_product
                                LEFT JOIN storage s 
		                            ON d.Storage_id = s.id_storage
                                LEFT JOIN storage_location sl 
		                            ON s.storage_location_id = sl.id_storage_location
                                LEFT JOIN city_and_zip_code czs 
		                            ON sl.City_and_zip_code_id = czs.id_city

                                WHERE (d.booking_date IS NULL)
                                AND (d.submission_cancellation_date IS NULL)
                                AND (d.booking_cancellation_date IS NULL)
        
	                            AND (product_origin_id = @ProductOriginId OR @ProductOriginId = 0)
                                AND (Storage_id = @StorageId OR @StorageId = 0)
                                AND (Product_id = @ProductId OR @ProductId = 0)
                                AND (d.City_and_zip_code_id = @City OR @City = 0)
                                AND (d.City_and_zip_code_id = @ZipCode OR @ZipCode = 0)

                                ORDER BY expiration_date
                                ";
            cmd.Parameters.Add(new MySqlParameter("ProductOriginId", ProductOriginId));
            cmd.Parameters.Add(new MySqlParameter("ProductId", ProductId));
            cmd.Parameters.Add(new MySqlParameter("StorageId", StorageId));
            cmd.Parameters.Add(new MySqlParameter("City", City));
            cmd.Parameters.Add(new MySqlParameter("ZipCode", ZipCode));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Donation donation = DataReaderToEntity(dr);

                    DonationDetails donationDetails = new DonationDetails(donation);

                    if (!dr.IsDBNull(dr.GetOrdinal("unit_name")))
                        donationDetails.UnitName = dr.GetString(dr.GetOrdinal("unit_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("recipient_last_name")))
                        donationDetails.RecipientLastName = dr.GetString(dr.GetOrdinal("recipient_last_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("recipient_first_name")))
                        donationDetails.RecipientFirstName = dr.GetString(dr.GetOrdinal("recipient_first_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("city_name_donation")))
                        donationDetails.CzCityName = dr.GetString(dr.GetOrdinal("city_name_donation"));
                    if (!dr.IsDBNull(dr.GetOrdinal("zip_code_donation")))
                        donationDetails.CzZipCode = dr.GetString(dr.GetOrdinal("zip_code_donation"));
                    if (!dr.IsDBNull(dr.GetOrdinal("product_origin_name")))
                        donationDetails.ProductOriginName = dr.GetString(dr.GetOrdinal("product_origin_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("product_name")))
                        donationDetails.ProductName = dr.GetString(dr.GetOrdinal("product_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("storage_name")))
                        donationDetails.StorageName = dr.GetString(dr.GetOrdinal("storage_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("storage_location_name")))
                        donationDetails.StorageLocationName = dr.GetString(dr.GetOrdinal("storage_location_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("storage_location_address")))
                        donationDetails.StorageLocationAddress = dr.GetString(dr.GetOrdinal("storage_location_address"));
                    if (!dr.IsDBNull(dr.GetOrdinal("city_name_storage")))
                        donationDetails.StorageLocationCityName = dr.GetString(dr.GetOrdinal("city_name_storage"));
                    if (!dr.IsDBNull(dr.GetOrdinal("zip_code_storage")))
                        donationDetails.StorageLocationZipCode = dr.GetString(dr.GetOrdinal("zip_code_storage"));

                    result.Add(donationDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(
                    "Erreur lors de la récupération des Dons : "
                    + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }

        public List<DonationDetails> GetByRecipient(int RecipientId)
        {
            List<DonationDetails> result = new List<DonationDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT d.* ,u.name 'unit_name',czd.city 'city_name_donation', czd.zip_code 'zip_code_donation',
                                            po.product_origin 'product_origin_name', p.name 'product_name',
                                            s.name 'storage_name',md.last_name 'recipient_last_name', md.first_name 'recipient_first_name', sl.name 'storage_location_name',
                                            sl.street_name_and_number 'storage_location_address', czs.city 'city_name_storage', czs.zip_code 'zip_code_storage'

	                            FROM donation d
	                            LEFT JOIN unit u 
		                            ON d.Unit_id = u.id_unit 
	                            LEFT JOIN member md 
		                            ON d.recipient_id = md.id_member
	                            LEFT JOIN city_and_zip_code czd 
		                            ON d.City_and_zip_code_id = czd.id_city
	                            LEFT JOIN product_origin po 
		                            ON d.Product_origin_id = po.id_origin
	                            LEFT JOIN product p 
		                            ON d.Product_id = p.id_product
                                LEFT JOIN storage s 
		                            ON d.Storage_id = s.id_storage
                                LEFT JOIN storage_location sl 
		                            ON s.storage_location_id = sl.id_storage_location
                                LEFT JOIN city_and_zip_code czs 
		                            ON sl.City_and_zip_code_id = czs.id_city

                                WHERE (d.booking_date IS NOT NULL)
                                AND (d.pick_up_date IS NULL)
                                AND (d.submission_cancellation_date IS NULL)
                                AND (d.booking_cancellation_date IS NULL)

	                            AND (d.recipient_id = @RecipientId OR @RecipientId = 0)
                                
                              
                                ORDER BY recipient_first_name
                                ";
            cmd.Parameters.Add(new MySqlParameter("RecipientId", RecipientId));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Donation donation = DataReaderToEntity(dr);

                    DonationDetails donationDetails = new DonationDetails(donation);


                    if (!dr.IsDBNull(dr.GetOrdinal("unit_name")))
                        donationDetails.UnitName = dr.GetString(dr.GetOrdinal("unit_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("recipient_last_name")))
                        donationDetails.RecipientLastName = dr.GetString(dr.GetOrdinal("recipient_last_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("recipient_first_name")))
                        donationDetails.RecipientFirstName = dr.GetString(dr.GetOrdinal("recipient_first_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("city_name_donation")))
                        donationDetails.CzCityName = dr.GetString(dr.GetOrdinal("city_name_donation"));
                    if (!dr.IsDBNull(dr.GetOrdinal("zip_code_donation")))
                        donationDetails.CzZipCode = dr.GetString(dr.GetOrdinal("zip_code_donation"));
                    if (!dr.IsDBNull(dr.GetOrdinal("product_origin_name")))
                        donationDetails.ProductOriginName = dr.GetString(dr.GetOrdinal("product_origin_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("product_name")))
                        donationDetails.ProductName = dr.GetString(dr.GetOrdinal("product_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("storage_name")))
                        donationDetails.StorageName = dr.GetString(dr.GetOrdinal("storage_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("storage_location_name")))
                        donationDetails.StorageLocationName = dr.GetString(dr.GetOrdinal("storage_location_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("storage_location_address")))
                        donationDetails.StorageLocationAddress = dr.GetString(dr.GetOrdinal("storage_location_address"));
                    if (!dr.IsDBNull(dr.GetOrdinal("city_name_storage")))
                        donationDetails.StorageLocationCityName = dr.GetString(dr.GetOrdinal("city_name_storage"));
                    if (!dr.IsDBNull(dr.GetOrdinal("zip_code_storage")))
                        donationDetails.StorageLocationZipCode = dr.GetString(dr.GetOrdinal("zip_code_storage"));

                    result.Add(donationDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(
                    "Erreur lors de la récupération des Dons : "
                    + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        #endregion
        #region GETBYID
        public DonationDetails GetDetailsById(int id)
        {
            DonationDetails donationDetails = null;

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT d.* ,u.name 'unit_name',md.last_name 'recipient_last_name', md.first_name 'recipient_first_name',
                                            cz.city 'city_name', cz.zip_code 'zip_code', 
                                            po.product_origin 'product_origin_name', p.name 'product_name'
                                            
	                            FROM donation d
	                            LEFT JOIN unit u 
		                            ON d.Unit_id = u.id_unit 
	                            LEFT JOIN member md 
		                            ON d.donor_id = md.id_member
	                            LEFT JOIN city_and_zip_code cz 
		                            ON d.City_and_zip_code_id = cz.id_city
	                            LEFT JOIN product_origin po 
		                            ON d.Product_origin_id = po.id_origin
	                            LEFT JOIN product p 
		                            ON d.Product_id = p.id_product
                                
                                WHERE id_donation = @id
                                
                ";
            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    Donation donation = DataReaderToEntity(dr);
                    donationDetails = new DonationDetails(donation);

                    if (!dr.IsDBNull(dr.GetOrdinal("unit_name")))
                        donationDetails.UnitName = dr.GetString(dr.GetOrdinal("unit_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("recipient_last_name")))
                        donationDetails.RecipientLastName = dr.GetString(dr.GetOrdinal("recipient_last_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("recipient_first_name")))
                        donationDetails.RecipientFirstName = dr.GetString(dr.GetOrdinal("recipient_first_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("city_name")))
                        donationDetails.CzCityName = dr.GetString(dr.GetOrdinal("city_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("zip_code")))
                        donationDetails.CzZipCode = dr.GetString(dr.GetOrdinal("zip_code"));
                    if (!dr.IsDBNull(dr.GetOrdinal("product_origin_name")))
                        donationDetails.ProductOriginName = dr.GetString(dr.GetOrdinal("product_origin_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("product_name")))
                        donationDetails.ProductName = dr.GetString(dr.GetOrdinal("product_name"));
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return donationDetails;
        }
        #endregion

        private Donation DataReaderToEntity(DbDataReader dr)
        {
            Donation donation = new Donation();

            donation.Id = dr.GetInt32(dr.GetOrdinal("id_donation"));
            donation.UnitId = dr.GetInt32(dr.GetOrdinal("unit_id"));
            donation.DonorId = dr.GetInt32(dr.GetOrdinal("donor_id"));
            if (!dr.IsDBNull(dr.GetOrdinal("storage_id")))
                donation.StorageId = dr.GetInt32(dr.GetOrdinal("storage_id"));
            if (!dr.IsDBNull(dr.GetOrdinal("recipient_id")))
                donation.RecipientId = dr.GetInt32(dr.GetOrdinal("recipient_id"));
            if (!dr.IsDBNull(dr.GetOrdinal("city_and_zip_code_id")))
                donation.CityAndZipCodeId = dr.GetInt32(dr.GetOrdinal("city_and_zip_code_id"));
            if (!dr.IsDBNull(dr.GetOrdinal("product_id")))
                donation.ProductId = dr.GetInt32(dr.GetOrdinal("product_id"));
            donation.ProductOriginId = dr.GetInt32(dr.GetOrdinal("product_origin_id"));
            if (!dr.IsDBNull(dr.GetOrdinal("expiration_date")))
                donation.ExpirationDate = dr.GetDateTime(dr.GetOrdinal("expiration_date"));
            if (!dr.IsDBNull(dr.GetOrdinal("quantity")))
                donation.Quantity = dr.GetFloat(dr.GetOrdinal("quantity"));
            if (!dr.IsDBNull(dr.GetOrdinal("description")))
                donation.Description = dr.GetString(dr.GetOrdinal("description"));
            if (!dr.IsDBNull(dr.GetOrdinal("picture")))
                donation.Picture = dr.GetString(dr.GetOrdinal("picture"));
            if (!dr.IsDBNull(dr.GetOrdinal("street_name_and_number")))
                donation.StreetNameNumber = dr.GetString(dr.GetOrdinal("street_name_and_number"));
            if (!dr.IsDBNull(dr.GetOrdinal("submission_date")))
                donation.SubmissionDate = dr.GetDateTime(dr.GetOrdinal("submission_date"));
            if (!dr.IsDBNull(dr.GetOrdinal("booking_date")))
                donation.BookingDate = dr.GetDateTime(dr.GetOrdinal("booking_date"));
            if (!dr.IsDBNull(dr.GetOrdinal("pick_up_date")))
                donation.PickUpDate = dr.GetDateTime(dr.GetOrdinal("pick_up_date"));
            if (!dr.IsDBNull(dr.GetOrdinal("booking_cancellation_date")))
                donation.BookingCancellationDate = dr.GetDateTime(dr.GetOrdinal("booking_cancellation_date"));
            if (!dr.IsDBNull(dr.GetOrdinal("submission_cancellation_date")))
                donation.SubmissionCancellationDate = dr.GetDateTime(dr.GetOrdinal("submission_cancellation_date"));

            return donation;
        }
           
    }
}
