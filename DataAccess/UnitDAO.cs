﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class UnitDAO : DAO
    {
        #region READ
        public Unit GetById(int id)
        {
            Unit result = new Unit();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * 
                                FROM unit 
                                WHERE id_unit = @id
                                ORDER BY name";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result.Id = dr.GetInt32(dr.GetOrdinal("id_unit"));
                    result.Name = dr.GetString(dr.GetOrdinal("name"));

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de la récupération de l'unité : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        public List<Unit> GetByProductId(int id)
        {
            List<Unit> results = new List<Unit>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT *
                                FROM unit u
                                    JOIN measures m ON u.id_unit = m.unit_id
                                WHERE m.product_id = @product_id
                                ORDER BY name";

            cmd.Parameters.Add(new MySqlParameter("product_id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Unit unit = new Unit();
                    unit.Id = dr.GetInt32(dr.GetOrdinal("id_unit"));
                    unit.Name = dr.GetString(dr.GetOrdinal("name"));

                    results.Add(unit);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de la récupération des unités applicables : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return results;
        }
#endregion

    }
}
