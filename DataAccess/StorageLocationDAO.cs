﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class StorageLocationDAO : DAO
    {
        public StorageLocation GetById(int storLocationId)
        {
            StorageLocation sl = null;
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT
                                 id_storage_location,Partner_id,City_and_zip_code_id,name,phone_number,
                                 picture,add_date,removal_date,street_name_and_number
                              FROM
                                 storage_location
                              WHERE
                                 id_storage_location = @id_storage_location ";
            cmd.Parameters.Add(new MySqlParameter("id_storage_location", storLocationId));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    sl = new StorageLocation();
                    sl.StorLocationId = dr.GetInt32(dr.GetOrdinal("id_storage_location"));
                    sl.PartnerId = dr.GetInt32(dr.GetOrdinal("Partner_id"));
                    sl.CityAndZipCodeId = dr.GetInt32(dr.GetOrdinal("City_and_zip_code_id"));
                    sl.Name = dr.GetString(dr.GetOrdinal("Name"));
                    sl.PhoneNumber = dr.GetString(dr.GetOrdinal("phone_number"));
                    sl.Picture = dr.GetString(dr.GetOrdinal("picture"));
                    sl.AddDate = dr.GetDateTime(dr.GetOrdinal("addDate"));
                    sl.RemovalDate = dr.GetDateTime(dr.GetOrdinal("remobal_time"));
                    sl.StreetNameAndNumber = dr.GetString(dr.GetOrdinal("street_name_and_number"));
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de creation d'un lieu de stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return sl;

        }
        public List<StorageLocation> GetAll()
        {
            List<StorageLocation> sls = new List<StorageLocation>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM storage_location";

            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                StorageLocation sl = DataReaderToEntity(dr);
                sls.Add(sl);
            }
            cnx.Close();

            return sls;
        }
        private StorageLocation DataReaderToEntity(DbDataReader dr)
        {
            StorageLocation sl = new StorageLocation();
            sl.StorLocationId = dr.GetInt32(dr.GetOrdinal("id_storage_location"));
            sl.PartnerId = dr.GetInt32(dr.GetOrdinal("Partner_id"));
            sl.CityAndZipCodeId = dr.GetInt32(dr.GetOrdinal("City_and_zip_code_id"));
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                sl.Name = dr.GetString(dr.GetOrdinal("name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("phone_number")))
            {
                sl.PhoneNumber = dr.GetString(dr.GetOrdinal("phone_number"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("picture")))
            {
                sl.Picture = dr.GetString(dr.GetOrdinal("picture"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("street_name_and_number")))
            {
                sl.StreetNameAndNumber = dr.GetString(dr.GetOrdinal("street_name_and_number"));
            }
            return sl;
        }
        public void Insert(StorageLocation storageLocation)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO storage_location
                              (Partner_id,City_and_zip_code_id,name,phone_number,
                              picture,add_date,removal_date,street_name_and_number)
                              VALUES
                              (@Partner_id,@City_and_zip_code_id,@name,@phone_number,
                               @picture,@add_date,@removal_date,@street_name_and_number)";
            cmd.Parameters.Add(new MySqlParameter("Partner_id", storageLocation.PartnerId));
            cmd.Parameters.Add(new MySqlParameter("City_and_zip_code_id", storageLocation.CityAndZipCodeId));
            cmd.Parameters.Add(new MySqlParameter("name", storageLocation.Name));
            cmd.Parameters.Add(new MySqlParameter("phone_number", storageLocation.PhoneNumber));
            cmd.Parameters.Add(new MySqlParameter("picture", storageLocation.Picture));
            cmd.Parameters.Add(new MySqlParameter("add_date", storageLocation.AddDate));
            cmd.Parameters.Add(new MySqlParameter("removal_date", storageLocation.RemovalDate));
            cmd.Parameters.Add(new MySqlParameter("street_name_and_number", storageLocation.StreetNameAndNumber));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de creation d'un lieu de stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void Update(StorageLocation storageLocation)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE storage_location
                               SET
                              Partner_id=@Partner_id,
                              City_and_zip_code_id=@City_and_zip_code_id,
                              name=@name,
                              phone_number=@phone_number,
                              picture=@picture,
                              add_date=@add_date,
                              removal_date=@removal_date,
                              street_name_and_number=@street_name_and_number
                              WHERE
                              id_storage_location=@id_storage_location";
            cmd.Parameters.Add(new MySqlParameter("Partner_id", storageLocation.PartnerId));
            cmd.Parameters.Add(new MySqlParameter("City_and_zip_code_id", storageLocation.CityAndZipCodeId));
            cmd.Parameters.Add(new MySqlParameter("name", storageLocation.Name));
            cmd.Parameters.Add(new MySqlParameter("phone_number", storageLocation.PhoneNumber));
            cmd.Parameters.Add(new MySqlParameter("picture", storageLocation.Picture));
            cmd.Parameters.Add(new MySqlParameter("add_date", storageLocation.AddDate));
            cmd.Parameters.Add(new MySqlParameter("removal_date", storageLocation.RemovalDate));
            cmd.Parameters.Add(new MySqlParameter("street_name_and_number", storageLocation.StreetNameAndNumber));
            cmd.Parameters.Add(new MySqlParameter("id_storage_location", storageLocation.StorLocationId));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de modification d'un lieu de stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void Desactive(StorageLocation storageLocation)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE storage_location
                               SET
                              removal_date=@removal_date 
                              WHERE
                              id_storage_location=@id_storage_location";
            cmd.Parameters.Add(new MySqlParameter("removal_date", storageLocation.RemovalDate));
            cmd.Parameters.Add(new MySqlParameter("id_storage_location", storageLocation.StorLocationId));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            } 
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de désactiviation d'un lieu de stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public List<StorageLocationDetails> GetByMultiCriteria(int PartnerId)
        {
            List<StorageLocationDetails> slds = new List<StorageLocationDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT s.*, p.appellation 'partner_name', c.city 'city_name'
                                FROM storage_location s
                                LEFT JOIN partner p
                                    ON s.Partner_id = p.id_partner
                                LEFT JOIN city_and_zip_code c
                                    ON s.City_and_zip_code_id = c.id_city
                                WHERE (s.removal_date IS NULL)
                                AND (Partner_id = @PartnerId OR @PartnerId = 0)                          
                              ";
            cmd.Parameters.Add(new MySqlParameter("PartnerId", PartnerId));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StorageLocation sl = DataReaderToEntity(dr);

                    StorageLocationDetails storageLocationDetails = new StorageLocationDetails(sl);

                    if (!dr.IsDBNull(dr.GetOrdinal("partner_name")))
                        storageLocationDetails.PartnerName = dr.GetString(dr.GetOrdinal("partner_name"));
                    if (!dr.IsDBNull(dr.GetOrdinal("city_name")))
                        storageLocationDetails.CityName = dr.GetString(dr.GetOrdinal("city_name"));
                    slds.Add(storageLocationDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(
                    "Erreur lors de la sélection d'un lieu de stockage : "
                    + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return slds;
        }
    }
}
