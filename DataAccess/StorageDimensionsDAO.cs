﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class StorageDimensionsDAO : DAO
    {

        public void Insert(StorageDimensions sd)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO storage_dimensions
                              (size_category,length,width,height)
                              VALUES
                              (@size_category,@length,@width,@height)";
            cmd.Parameters.Add(new MySqlParameter("size_category", sd.CategorySize));
            cmd.Parameters.Add(new MySqlParameter("length", sd.Length));
            cmd.Parameters.Add(new MySqlParameter("width", sd.Width));
            cmd.Parameters.Add(new MySqlParameter("height", sd.Height));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de creation d'un stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void Update(StorageDimensions sd)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE -storage_dimensions
                               SET 
                              size_category=@size_category,
                              length=@length,
                              width=@width,
                              height=@height
                              WHERE
                              id_dimensions=@id_dimensions";
            cmd.Parameters.Add(new MySqlParameter("size_category", sd.CategorySize));
            cmd.Parameters.Add(new MySqlParameter("length", sd.Length));
            cmd.Parameters.Add(new MySqlParameter("width", sd.Width));
            cmd.Parameters.Add(new MySqlParameter("height", sd.Height));
            cmd.Parameters.Add(new MySqlParameter("id_dimensions", sd.Id));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de modification d'un stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public List<StorageDimensions> GetAll()
        {
            List<StorageDimensions> sds = new List<StorageDimensions>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM storage_dimensions";

            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                StorageDimensions sd = DataReaderToEntity(dr);
                sds.Add(sd);
            }
            cnx.Close();

            return sds;
        }
        private StorageDimensions DataReaderToEntity(DbDataReader dr)
        {
            StorageDimensions sd = new StorageDimensions();
            sd.Id = dr.GetInt32(dr.GetOrdinal("id_dimensions"));
            if (!dr.IsDBNull(dr.GetOrdinal("size_category")))
            {
                sd.CategorySize = dr.GetString(dr.GetOrdinal("size_category"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("length")))
            {
                sd.Length = dr.GetInt32(dr.GetOrdinal("length"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("width")))
            {
                sd.Width = dr.GetInt32(dr.GetOrdinal("width"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("height")))
            {
                sd.Height = dr.GetInt32(dr.GetOrdinal("height"));
            }
            return sd;
        }
    }
}
