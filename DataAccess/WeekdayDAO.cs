﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class WeekdayDAO : DAO
    {
        public List<Weekday> GetAll()
        {
            List<Weekday> wDays = new List<Weekday>();
            DbConnection cnx= new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM weekday";

            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Weekday wday = DataReaderToEntity(dr);
                wDays.Add(wday);
            }
            cnx.Close();

            return wDays;
        }
        private Weekday DataReaderToEntity(DbDataReader dr)
        {
            Weekday wday = new Weekday();
            wday.Id = dr.GetInt32(dr.GetOrdinal("id_weekday"));
            wday.Name = dr.GetString(dr.GetOrdinal("weekday_name"));
            return wday;
        }
        public void Insert(Weekday wday)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO weekday
                              (id_weekday, weekday_name)
                              VALUES
                              (@id_weekday,@weekday_name)";
            cmd.Parameters.Add(new MySqlParameter("id_weekday", wday.Id));
            cmd.Parameters.Add(new MySqlParameter("weekday_name", wday.Name));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur d'ajoute des jours d'une semaine : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

        }
    }
}
