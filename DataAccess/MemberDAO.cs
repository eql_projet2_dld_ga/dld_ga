﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class MemberDAO : DAO
    {
        #region READ
        public List<Member> GetAll()
        {
            List<Member> result = new List<Member>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM member ORDER BY last_name, first_name";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Member member = DataReaderToEntity(dr);
                    result.Add(member);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des membres : " + ex.Message);
            }
            finally
            {
                cnx.Close();    
            }

            return result;
        }

        public MemberDetails GetDetailsById(int id)
        {
            MemberDetails result = new MemberDetails();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT m.*, c.city, c.zip_code
                                FROM member m
                                     JOIN city_and_zip_code c ON m.city_and_zip_code_id = c.id_city
                                WHERE m.id_member = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    Member member = DataReaderToEntity(dr);

                    result = new MemberDetails(member);

                    result.City = dr.GetString(dr.GetOrdinal("city"));
                    result.ZipCode = dr.GetString(dr.GetOrdinal("zip_code"));
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des détails du membre : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;

        }

        #region PRIVATE

        private static Member DataReaderToEntity(DbDataReader dr)
        {
            Member member = new Member();
            
            member.Id = dr.GetInt32(dr.GetOrdinal("id_member"));
            member.FirstName = dr.GetString(dr.GetOrdinal("first_name"));
            member.LastName = dr.GetString(dr.GetOrdinal("last_name"));
            member.BirthDate = dr.GetDateTime(dr.GetOrdinal("birth_date"));
            member.Email = dr.GetString(dr.GetOrdinal("email"));
            member.Password = dr.GetString(dr.GetOrdinal("password"));
            member.PhoneNumber = dr.GetString(dr.GetOrdinal("phone_number"));
            member.RegistrationDate = dr.GetDateTime(dr.GetOrdinal("registration_date"));
            member.ValidationCode = dr.GetInt32(dr.GetOrdinal("validation_code"));

            if (!dr.IsDBNull(dr.GetOrdinal("city_and_Zip_code_id")))
            { member.CityAndZipCodeId = dr.GetInt32(dr.GetOrdinal("city_and_Zip_code_id")); }
            if (!dr.IsDBNull(dr.GetOrdinal("street_name_and_number")))
            { member.StreetNameNumber = dr.GetString(dr.GetOrdinal("street_name_and_number")); }
            if (!dr.IsDBNull(dr.GetOrdinal("email_validation_date")))
            { member.EmailValidationDate = dr.GetDateTime(dr.GetOrdinal("email_validation_date")); }
            if (!dr.IsDBNull(dr.GetOrdinal("deregistration_date")))
            { member.DeregistrationDate = dr.GetDateTime(dr.GetOrdinal("deregistration_date")); }
            if (!dr.IsDBNull(dr.GetOrdinal("picture")))
            { member.Picture = dr.GetString(dr.GetOrdinal("picture")); }
            return member;
        }
        #endregion

        #endregion

    }
}
