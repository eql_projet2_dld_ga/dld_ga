﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class SubsetDAO : DAO
    {
        #region READ
        public List<Subset> GetBySubCategoryId(int id)
        {
            List<Subset> results = new List<Subset>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT *
                                FROM subset
                                WHERE subcategory_id = @subcategory_id
                                ORDER BY subset_name";

            cmd.Parameters.Add(new MySqlParameter("subcategory_id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Subset subset = new Subset();
                    subset.Id = dr.GetInt32(dr.GetOrdinal("id_subset"));
                    subset.SubcategoryId = dr.GetInt32(dr.GetOrdinal("subcategory_id"));
                    subset.Name = dr.GetString(dr.GetOrdinal("subset_name"));

                    results.Add(subset);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de la récupération des subsets de la subcategory : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return results;
        }
        #endregion
    }
}
