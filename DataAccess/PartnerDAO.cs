﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class PartnerDAO : DAO
    {
        public List<Partner> GetAll()
        {
            List<Partner> result = new List<Partner>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * FROM partner";

            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Partner partner = DataReaderToEntity(dr);
                result.Add(partner);
            }

            cnx.Close();

            return result;
        }
        private Partner DataReaderToEntity(DbDataReader dr)
        {
            Partner partner = new Partner();

            partner.PartnerId = dr.GetInt32(dr.GetOrdinal("id_partner"));
            partner.Appellation = dr.GetString(dr.GetOrdinal("appellation"));
            if (!dr.IsDBNull(dr.GetOrdinal("City_and_zip_code_Id")))
            {
                partner.CityAndZipCodeId = dr.GetInt32(dr.GetOrdinal("City_and_zip_code_Id"));
            }

            return partner;
        }
        public void Insert(Partner partner)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO partner
                              (City_and_zip_code_id,appellation,email,password,registration_date, 
                              validation_code,email_validation_date,admin_validation_date,
                              first_name,last_name,role,birth_date,phone_number,
                              deregistration_date,street_name_and_number)
                              VALUES
                              (@City_and_zip_code_id,@appellation,@email,@password,
                              @registration_date,@validation_code,@email_validation_date,
                              @admin_validation_date,@first_name,@last_name,@role,
                              @birth_date,@phone_number,@deregistration_date,
                              @street_name_and_number)";
            cmd.Parameters.Add(new MySqlParameter("City_and_zip_code_id", partner.CityAndZipCodeId));
            cmd.Parameters.Add(new MySqlParameter("appellation", partner.Appellation));
            cmd.Parameters.Add(new MySqlParameter("email", partner.Email));
            cmd.Parameters.Add(new MySqlParameter("password", partner.Password));
            cmd.Parameters.Add(new MySqlParameter("registration_date", partner.RegistrationDate));
            cmd.Parameters.Add(new MySqlParameter("validation_code", partner.ValidationCode));
            cmd.Parameters.Add(new MySqlParameter("email_validation_date", partner.EmailValidDate));
            cmd.Parameters.Add(new MySqlParameter("admin_validation_date", partner.AdmValidDate));
            cmd.Parameters.Add(new MySqlParameter("first_name", partner.FirstName));
            cmd.Parameters.Add(new MySqlParameter("last_name", partner.LastName));
            cmd.Parameters.Add(new MySqlParameter("role", partner.Role));
            cmd.Parameters.Add(new MySqlParameter("birth_date", partner.BirthDate));
            cmd.Parameters.Add(new MySqlParameter("phone_number", partner.PhoneNumber));
            cmd.Parameters.Add(new MySqlParameter("deregistration_date", partner.DeregistrDate));
            cmd.Parameters.Add(new MySqlParameter("street_name_and_number", partner.StreetNameAndNumber));
            
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de creation d'un Partenaire : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
    }
}
