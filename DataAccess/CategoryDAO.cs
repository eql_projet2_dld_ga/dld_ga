﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class CategoryDAO : DAO
    {
        #region READ
        public List<Category> GetAll()
        {
            List<Category> result = new List<Category>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM category ORDER BY category_name";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Category category = new Category();
                    category.Id = dr.GetInt32(dr.GetOrdinal("id_category"));
                    category.Name = dr.GetString(dr.GetOrdinal("category_name"));
                    result.Add(category);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des catégories : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        #endregion
    }
}
