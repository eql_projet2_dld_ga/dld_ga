﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class ProductOriginDAO : DAO
    {
        public List<ProductOrigin> GetAll()
        {
            List<ProductOrigin> result = new List<ProductOrigin>();

            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM product_origin ORDER BY product_origin";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    ProductOrigin origin = new ProductOrigin();

                    origin.Id = dr.GetInt32(dr.GetOrdinal("id_origin"));
                    origin.Origin = dr.GetString(dr.GetOrdinal("product_origin"));
                    result.Add(origin);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération de l'origine du produit : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }

        
    }
}
