﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class OpeningHoursDAO : DAO
    {

        #region CREATE
        public void Insert(OpeningHours openingHours)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();

            if (openingHours.DonationId > 0)
            {
                cmd.CommandText = @"INSERT INTO opening_hours
                             (Weekday_id,Donation_id,opening_hour,opening_minute,closing_hour,
                              closing_minute,break_start_hour,break_start_minute,
                              break_end_hour,break_end_minute)
                             VALUES
                             (@Weekday_id,@Donation_id,@opening_hour,@opening_minute,@closing_hour,
                              @closing_minute,@break_start_hour,@break_start_minute,
                              @break_end_hour,@break_end_minute)";

                cmd.Parameters.Add(new MySqlParameter("Donation_id", openingHours.DonationId));
            } 
            else if (openingHours.StorLocationId > 0)
            {
                cmd.CommandText = @"INSERT INTO opening_hours
                             (Weekday_id,Storage_location_id,opening_hour,opening_minute,closing_hour,
                              closing_minute,break_start_hour,break_start_minute,
                              break_end_hour,break_end_minute)
                             VALUES
                             (@Weekday_id,@Storage_location_id,@opening_hour,@opening_minute,@closing_hour,
                              @closing_minute,@break_start_hour,@break_start_minute,
                              @break_end_hour,@break_end_minute)";
                cmd.Parameters.Add(new MySqlParameter("Storage_location_id", openingHours.StorLocationId));
            } 
            else if (openingHours.StorId > 0)
            {
                cmd.CommandText = @"INSERT INTO opening_hours
                             (Weekday_id,Storage_id,opening_hour,opening_minute,closing_hour,
                              closing_minute,break_start_hour,break_start_minute,
                              break_end_hour,break_end_minute)
                             VALUES
                             (@Weekday_id,@Storage_id,@opening_hour,@opening_minute,@closing_hour,
                              @closing_minute,@break_start_hour,@break_start_minute,
                              @break_end_hour,@break_end_minute)";
                cmd.Parameters.Add(new MySqlParameter("Storage_id", openingHours.StorId));
            }
            
            cmd.Parameters.Add(new MySqlParameter("Weekday_id", openingHours.WeekdayId));
            cmd.Parameters.Add(new MySqlParameter("opening_hour", openingHours.OpeningHour));
            cmd.Parameters.Add(new MySqlParameter("opening_minute", openingHours.OpeningMinute));
            cmd.Parameters.Add(new MySqlParameter("closing_hour", openingHours.ClosingHour));
            cmd.Parameters.Add(new MySqlParameter("closing_minute", openingHours.ClosingMinute));
            cmd.Parameters.Add(new MySqlParameter("break_start_hour", openingHours.BreakStartHour));
            cmd.Parameters.Add(new MySqlParameter("break_start_minute", openingHours.BreakStartMinute));
            cmd.Parameters.Add(new MySqlParameter("break_end_hour", openingHours.BreakEndHour));
            cmd.Parameters.Add(new MySqlParameter("break_end_minute", openingHours.BreakEndMinute));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de creation d'une horaire d'ouverture : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

        }
        #endregion

        #region READ
        public List<OpeningHoursDetails> GetAllDetailsByDonationId(int donId)
        {
            List<OpeningHoursDetails> results = new List<OpeningHoursDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            
            cmd.CommandText = @"SELECT w.weekday_name, oh.*
                                FROM opening_hours oh
                                    JOIN weekday w ON oh.weekday_id = w.id_weekday
                                WHERE 
	                                donation_id = @id
                                ORDER BY weekday_id
                                ";
            cmd.Parameters.Add(new MySqlParameter("id", donId));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    OpeningHours tempHours = DataReaderToEntity(dr);
                    OpeningHoursDetails hours = new OpeningHoursDetails(tempHours);

                    hours.Weekday = dr.GetString(dr.GetOrdinal("weekday_name"));

                    results.Add(hours);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des horaires du don : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return results;
        }

        private OpeningHours DataReaderToEntity(DbDataReader dr)
        {
            OpeningHours hours = new OpeningHours();

            hours.BusinessHoursId = dr.GetInt32(dr.GetOrdinal("id_business_hours"));
            hours.WeekdayId = dr.GetInt32(dr.GetOrdinal("weekday_id"));

            hours.OpeningHour = dr.GetInt32(dr.GetOrdinal("opening_hour"));
            hours.ClosingHour = dr.GetInt32(dr.GetOrdinal("closing_hour"));

            if (!dr.IsDBNull(dr.GetOrdinal("donation_id")))
                hours.DonationId = dr.GetInt32(dr.GetOrdinal("donation_id"));
            if (!dr.IsDBNull(dr.GetOrdinal("storage_location_id")))
                hours.StorLocationId = dr.GetInt32(dr.GetOrdinal("storage_location_id"));

            if (!dr.IsDBNull(dr.GetOrdinal("opening_minute")))
                hours.OpeningMinute = dr.GetInt32(dr.GetOrdinal("opening_minute"));
            if (!dr.IsDBNull(dr.GetOrdinal("closing_minute")))
                hours.ClosingMinute = dr.GetInt32(dr.GetOrdinal("closing_minute"));

            if (!dr.IsDBNull(dr.GetOrdinal("break_start_hour")))
                hours.BreakStartHour = dr.GetInt32(dr.GetOrdinal("break_start_hour"));
            if (!dr.IsDBNull(dr.GetOrdinal("break_start_minute")))
                hours.BreakStartMinute = dr.GetInt32(dr.GetOrdinal("break_start_minute"));
            if (!dr.IsDBNull(dr.GetOrdinal("break_end_hour")))
                hours.BreakEndHour = dr.GetInt32(dr.GetOrdinal("break_end_hour"));
            if (!dr.IsDBNull(dr.GetOrdinal("break_end_minute")))
                hours.BreakEndMinute = dr.GetInt32(dr.GetOrdinal("break_end_minute"));

            return hours;
        }
        #endregion
    }
}
