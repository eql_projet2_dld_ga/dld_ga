﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class CityAndZipCodeDAO : DAO
    {
        public List<CityAndZipCode> GetAll()
        {
            List<CityAndZipCode> result = new List<CityAndZipCode>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM city_and_zip_code";

            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                CityAndZipCode cazd = DataReaderToEntity(dr);
                result.Add(cazd);
            }
            cnx.Close();

            return result;
        }
        private CityAndZipCode DataReaderToEntity(DbDataReader dr)
        {
            CityAndZipCode cazd = new CityAndZipCode();
            cazd.CityId = dr.GetInt32(dr.GetOrdinal("id_city"));
            cazd.City = dr.GetString(dr.GetOrdinal("city"));
            cazd.ZipCode = dr.GetString(dr.GetOrdinal("zip_code"));
            return cazd;
        }
        public void Insert(CityAndZipCode cityAndZipCode)
        {
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO city_and_zip_code
                              (id_city, city, zip_code)
                              VALUES
                              (@id_city,@city,@zip_code)";
            cmd.Parameters.Add(new MySqlParameter("id_city", cityAndZipCode.CityId));
            cmd.Parameters.Add(new MySqlParameter("city", cityAndZipCode.City));
            cmd.Parameters.Add(new MySqlParameter("zip_code", cityAndZipCode.ZipCode));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur d'insertion d'une ville et un code postal correspondant : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
    }
}
