﻿using Fr.EQL.AI110.DLD_GA.Entities;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.DLD_GA.DataAccess
{
    public class StorageAccessibilityDAO : DAO
    {
        public List<StorageAccessibility> GetAll()
        {
            List<StorageAccessibility> sas = new List<StorageAccessibility>();
            DbConnection cnx = new MySqlConnection(CNX_STRING);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM storage_accessibility";
            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                StorageAccessibility sa = DataReaderToEntity(dr);
                sas.Add(sa);
            }
            cnx.Close();
            return sas;
        }
        private StorageAccessibility DataReaderToEntity(DbDataReader dr)
        {
            StorageAccessibility sa = new StorageAccessibility();
            sa.Id = dr.GetInt32(dr.GetOrdinal("id_accessibility"));
            if (!dr.IsDBNull(dr.GetOrdinal("accessibility")))
            {
                sa.Accessibility = dr.GetString(dr.GetOrdinal("accessibility"));
            }
            
            return sa;
        }
    }
}
