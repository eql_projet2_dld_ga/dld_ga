﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class SubCategory
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public String Name { get; set; }
        public List<Subset> Subsets { get; set; }
        #endregion
    }
}
