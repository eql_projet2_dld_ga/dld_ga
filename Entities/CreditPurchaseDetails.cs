﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class CreditPurchaseDetails : CreditPurchase
    {
        public string MemberFirstName { get; set; }
        public string MemberLastName { get; set; }
        public string BundleName { get; set; }
        public int BundleCreditNumber { get; set; }
        public float BundlePrice { get; set; }
        
    }
}
