﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Member
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public int CityAndZipCodeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string StreetNameNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string Picture { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int ValidationCode { get; set; }
        public DateTime EmailValidationDate { get; set; }
        public DateTime DeregistrationDate { get; set; }
#endregion


    }
}
