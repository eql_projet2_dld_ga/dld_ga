﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Weekday
    {
        #region Attributs
        public int Id { get; set; }
        public string Name { get; set; }
        #endregion Attributs

        #region Constructeur
        public Weekday()
        {

        }
        public Weekday(int id, string name)
        {
            Id = id;
            Name = name;
        }
        #endregion Constructeur
    }
}
