﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Product
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public int SubsetId { get; set; }
        public int PreservationId { get; set; }
        public string Name { get; set; }
        public List<Unit> Units { get; set; }
        #endregion

    }
}
