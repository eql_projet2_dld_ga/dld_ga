﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class StorageAccessibility
    {
        #region ATTRIBUTS   
        public int Id { get; set; }
        public string Accessibility { get; set; }
        #endregion

        #region Constructeur
        public StorageAccessibility()
        {

        }
        public StorageAccessibility(int id, string accessibility)
        {
            Id = id;
            Accessibility = accessibility;
        }
        #endregion Constructeur
    }
}
