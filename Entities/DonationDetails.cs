﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class DonationDetails : Donation
    {
        #region CONSTRUCTORS
        public DonationDetails()
        {

        }
        public DonationDetails(Donation donation)
        {
            Id = donation.Id;
            UnitId = donation.UnitId;
            DonorId = donation.DonorId;
            StorageId = donation.StorageId;
            RecipientId = donation.RecipientId;
            CityAndZipCodeId = donation.CityAndZipCodeId;
            ProductId = donation.ProductId;
            ProductOriginId = donation.ProductOriginId;
            ExpirationDate = donation.ExpirationDate;
            Quantity = donation.Quantity;
            Description = donation.Description;
            Picture = donation.Picture;
            StreetNameNumber = donation.StreetNameNumber;
            SubmissionDate = donation.SubmissionDate;
            BookingDate = donation.BookingDate;
            PickUpDate = donation.PickUpDate;
            BookingCancellationDate = donation.BookingCancellationDate;
            SubmissionCancellationDate = donation.SubmissionCancellationDate;
        }
        #endregion
        public string UnitName { get; set; }
        public string RecipientLastName { get; set; }
        public string RecipientFirstName { get; set; }
        public string CzCityName { get; set; }
        public string CzZipCode { get; set; }
        public string ProductOriginName { get; set; }
        public string ProductName { get; set; }
        public string StorageName { get; set; }
        public string StorageLocationName { get; set; }
        public string StorageLocationCityName { get; set; }
        public string StorageLocationZipCode { get; set; }
        public string StorageLocationAddress { get; set; }
        public string ProductPreservationId { get; set; }
        public string Productpreservation { get; set; }
        

    }
}
