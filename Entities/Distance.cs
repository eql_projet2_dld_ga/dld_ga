﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Distance
    {
        #region ATTRIBUTS
        public int IdCityOrigin { get; set; }
        public int IdCityDistance { get; set; }
        public float AverageDistance { get; set; }
        #endregion

    }
}
