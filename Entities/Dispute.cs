﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Dispute
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public int OffenderPartnerId { get; set; }
        public int OffenderMemberId { get; set; }
        public int ComplainantPartnerId { get; set; }
        public int ComplainantMemberId { get; set; }
        public int DonationId { get; set; }
        public int DisputeCategoryId { get; set; }
        public DateTime DisputeDate { get; set; }
        public string OpenDescription { get; set; }
        public DateTime HandlingStartDate { get; set; }
        public DateTime HandlingEndDate { get; set; }
        public DateTime RecipientRefundDate { get; set; }
        public DateTime DonorCreditCancellationDate { get; set; }
        #endregion
    }
}
