﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Unavailabilty
    {
        #region Attributs
        public int UnavailabilityId { get; set; }
        public int StorLocationId { get; set; }
        public int AbsentId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DeclaDate { get; set; }
        public DateTime CancelDate { get; set; }

        #endregion Attributs

        #region Constructeurs

        #endregion Constructeurs
    }
}
