﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class ProductOrigin
    {
        #region ATTRIBUTS
        public int Id { get; set; }
        public string Origin { get; set; }
        #endregion

    }
}