﻿namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class OpeningHours
    {
        #region Attributs
        public int BusinessHoursId { get; set; }
        public int WeekdayId { get; set; }
        public int? DonationId { get; set; }
        public int StorLocationId { get; set; }
        public int? StorId { get; set; }
        public int OpeningHour { get; set; }
        public int OpeningMinute { get; set; }
        public int ClosingHour { get; set; }
        public int ClosingMinute { get; set; }
        public int BreakStartHour { get; set; }
        public int BreakStartMinute { get; set; }
        public int BreakEndHour { get; set; }
        public int BreakEndMinute { get; set; }

        #endregion Attributs

        #region Constructeurs
        public OpeningHours()
        {

        }
        public OpeningHours(int businessHoursId, int weekdayId, int donationId,
            int storLocationId, int storId, int openingHour, int openingMinute,
            int closingHour, int closingMinute,int breakStartHour, int breakStartMinute,
            int breakEndHour, int breakEndMinute)
        {
            BusinessHoursId = businessHoursId;
            WeekdayId = weekdayId;
            DonationId = donationId;
            StorLocationId = storLocationId;
            StorId = storId;
            OpeningHour = openingHour;
            OpeningMinute = openingMinute;
            ClosingHour = closingHour;
            ClosingMinute = closingMinute;
            BreakStartHour = breakStartHour;
            BreakStartMinute = breakStartMinute;
            BreakEndHour = breakEndHour;
            BreakEndMinute = breakEndMinute;           
        }
        public OpeningHours(int businessHoursId, int weekdayId, int donationId,
            int storLocationId, int storId,int openingHour,int closingHour,
            int breakStartHour,int breakEndHour)
        {
            BusinessHoursId = businessHoursId;
            WeekdayId = weekdayId;
            DonationId= donationId;
            StorLocationId = storLocationId;
            StorId= storId;
            OpeningHour = openingHour;
            ClosingHour = closingHour;
            BreakStartHour = breakStartHour;
            BreakEndHour = breakEndHour;
        }
        public OpeningHours(int businessHoursId, int weekdayId,int storId, 
                        int openingHour, int openingMinute, int closingHour, int closingMinute, 
                        int breakStartHour, int breakStartMinute,
                        int breakEndHour, int breakEndMinute)
        {
            BusinessHoursId = businessHoursId;
            WeekdayId = weekdayId;
            StorId = storId;
            OpeningHour = openingHour;
            OpeningMinute = openingMinute;
            ClosingHour = closingHour;
            ClosingMinute = closingMinute;
            BreakStartHour = breakStartHour;
            BreakStartMinute= breakStartMinute;
            BreakEndHour = breakEndHour;
            BreakStartMinute= breakEndMinute;
        }
        public OpeningHours(int businessHoursId, int weekdayId, int storLocationId, int storId,
                int openingHour, int openingMinute, int closingHour, int closingMinute,
                int breakStartHour, int breakStartMinute,
                int breakEndHour, int breakEndMinute)
        {
            BusinessHoursId = businessHoursId;
            WeekdayId = weekdayId;
            StorLocationId= storLocationId;
            StorId = storId;
            OpeningHour = openingHour;
            OpeningMinute = openingMinute;
            ClosingHour = closingHour;
            ClosingMinute = closingMinute;
            BreakStartHour = breakStartHour;
            BreakStartMinute = breakStartMinute;
            BreakEndHour = breakEndHour;
            BreakStartMinute = breakEndMinute;
        }
        public OpeningHours(int businessHoursId, int weekdayId, int storLocationId,
          int openingHour, int closingHour, int breakStartHour, int breakEndHour)
        {
            BusinessHoursId = businessHoursId;
            WeekdayId = weekdayId;
            StorLocationId = storLocationId;
            OpeningHour = openingHour;
            ClosingHour = closingHour;
            BreakStartHour = breakStartHour;
            BreakEndHour = breakEndHour;
        }
        public OpeningHours(int businessHoursId, int weekdayId, int storLocationId, int storId,
                    int openingHour, int closingHour, int breakStartHour, int breakEndHour)
        {
            BusinessHoursId = businessHoursId;
            WeekdayId = weekdayId;
            StorLocationId = storLocationId;
            StorId= storId;
            OpeningHour = openingHour;
            ClosingHour = closingHour;
            BreakStartHour = breakStartHour;
            BreakEndHour = breakEndHour;
        }
        public OpeningHours(int businessHoursId, int weekdayId, int storLocationId,
                            int openingHour, int closingHour)
        {
            BusinessHoursId = businessHoursId;
            WeekdayId = weekdayId;
            StorLocationId = storLocationId;
            OpeningHour = openingHour;
            ClosingHour = closingHour;
        }
        public OpeningHours(int businessHoursId, int weekdayId, int storLocationId,
                            int storId,int openingHour, int closingHour)
        {
            BusinessHoursId = businessHoursId;
            WeekdayId = weekdayId;
            StorLocationId = storLocationId;
            StorId = storId;
            OpeningHour = openingHour;
            ClosingHour = closingHour;
        }
        #endregion Constructeurs
    }
}
