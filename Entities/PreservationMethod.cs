﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class PreservationMethod
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public string Method { get; set; }
        #endregion

        #region Constructeurs
        public PreservationMethod()
        {

        }
        public PreservationMethod(int id, string method)
        {
            Id = id;
            Method = method;
        }
        #endregion Constructeurs
    }
}
