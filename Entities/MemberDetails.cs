﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class MemberDetails : Member
    {
        #region ATTRIBUTES
        public string City { get; set; }
        public string ZipCode { get; set; }
        public int CreditTotal { get; set; }
        #endregion

        #region CONSTRUCTORS
        public MemberDetails()
        {

        }
        public MemberDetails(Member member)
        {
            Id = member.Id;
            CityAndZipCodeId = member.CityAndZipCodeId;
            FirstName = member.FirstName;
            LastName = member.LastName;
            BirthDate = member.BirthDate;
            StreetNameNumber = member.StreetNameNumber;
            Email = member.Email;
            PhoneNumber = member.PhoneNumber;
            Picture = member.Picture;
            RegistrationDate = member.RegistrationDate;
            EmailValidationDate = member.EmailValidationDate;
            DeregistrationDate = member.DeregistrationDate;
        }

        public MemberDetails(Member member, string city, string zipcode)
        {
            Id = member.Id;
            CityAndZipCodeId = member.CityAndZipCodeId;
            FirstName = member.FirstName;
            LastName = member.LastName;
            BirthDate = member.BirthDate;
            StreetNameNumber = member.StreetNameNumber;
            Email = member.Email;
            PhoneNumber = member.PhoneNumber;
            Picture = member.Picture;
            RegistrationDate = member.RegistrationDate;
            EmailValidationDate = member.EmailValidationDate;
            DeregistrationDate = member.DeregistrationDate;

            City = city;
            ZipCode = zipcode;
        }
        #endregion
    }
}
