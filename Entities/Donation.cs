﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Donation
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public int UnitId { get; set; }
        public int DonorId { get; set; }
        public int StorageId { get; set; }
        public int RecipientId { get; set; }
        public int CityAndZipCodeId { get; set; }
        public int ProductId { get; set; }
        public int ProductOriginId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public float Quantity { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public string StreetNameNumber { get; set; }

        #region STATUS ATTRIBUTES
        public DateTime SubmissionDate { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime PickUpDate { get; set; }
        public DateTime BookingCancellationDate { get; set; }
        public DateTime SubmissionCancellationDate { get; set; }
        
        #endregion

        #endregion

        #region CONTRUCTOR
        public Donation()
        {

        }
        public Donation(int id, int unitId, int donorId, int storageId, int recipientId, int cityAndZipCodeId, int productId, int productOriginId, DateTime expirationDate, int quantity, string description, string picture, string streetNameNumber, DateTime submissionDate, DateTime bookingDate, DateTime pickUpDate, DateTime bookingCancellationDate, DateTime submissionCancellationDate)
        {
            Id = id;
            UnitId = unitId;
            DonorId = donorId;
            StorageId = storageId;
            RecipientId = recipientId;
            CityAndZipCodeId = cityAndZipCodeId;
            ProductId = productId;
            ProductOriginId = productOriginId;
            ExpirationDate = expirationDate;
            Quantity = quantity;
            Description = description;
            Picture = picture;
            StreetNameNumber = streetNameNumber;
            SubmissionDate = submissionDate;
            BookingDate = bookingDate;
            PickUpDate = pickUpDate;
            BookingCancellationDate = bookingCancellationDate;
            SubmissionCancellationDate = submissionCancellationDate;
        }
        #endregion
    }
}
