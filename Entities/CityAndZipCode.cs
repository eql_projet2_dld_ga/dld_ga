﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class CityAndZipCode
    {
        #region ATTRIBUTS
        public int CityId { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        #endregion

        #region Constructeurs
        public CityAndZipCode()
        {

        }
        public CityAndZipCode(int cityId, string city, string zipCode)
        {
            CityId = cityId;
            City = city;
            ZipCode = zipCode;
        }
        #endregion

    }
}
