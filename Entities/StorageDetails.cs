﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class StorageDetails : Storage
    {
        #region ATTRIBUTES
        public string StorageLocationName { get; set; }
        public string StreetNameNumber { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Accessibility { get; set; }
        public string Dimensions { get; set; }
        public string StoragePreservationMethod { get; set; }
        public string MethodName { get; set; }
        public string LocationName { get; set; }
        public string DimensionName { get; set; }
        public string AccesName { get; set; }
        #endregion
        #region CONSTRUCTORS
        public StorageDetails() 
        {
        }
        public StorageDetails(Storage storage)
        {
            StorId = storage.StorId;
            PreservationMethId = storage.PreservationMethId;
            StorLocationId = storage.StorLocationId;
            StorageDimensionsId = storage.StorageDimensionsId;
            StorageAccessibilityId = storage.StorageAccessibilityId;
            Name = storage.Name;
            //AddDate = storage.AddDate;
            //RemovalDate = storage.RemovalDate;
         }

        #endregion

    }
}
