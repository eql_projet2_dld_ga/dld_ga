﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class StorageLocation
    {
        #region Attributs
        public int StorLocationId { get; set; }
        public int PartnerId { get; set; }
        public int CityAndZipCodeId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Picture { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? RemovalDate { get; set; }
        public string StreetNameAndNumber { get; set; }

        #endregion Attributs

        #region Constructeurs
        public StorageLocation()
        {

        }

        public StorageLocation(int storLocationId, 
            int partnerId, int cityAndZipCodeId,string name, string phoneNumber,
            string picture, DateTime addDate, DateTime removalDate,
            string streetNameAndNumber)
        {
            StorLocationId = storLocationId;
            PartnerId = partnerId;
            CityAndZipCodeId = cityAndZipCodeId;
            Name = name;
            PhoneNumber = phoneNumber;
            Picture = picture;
            AddDate = addDate;
            RemovalDate = removalDate;
            StreetNameAndNumber = streetNameAndNumber;
        }
        public StorageLocation(int storLocationId,
        int partnerId, int cityAndZipCodeId, string name, string phoneNumber,
        string picture, DateTime addDate, string streetNameAndNumber)
        {
            StorLocationId = storLocationId;
            PartnerId = partnerId;
            CityAndZipCodeId = cityAndZipCodeId;
            Name = name;
            PhoneNumber = phoneNumber;
            Picture = picture;
            AddDate = addDate;
            StreetNameAndNumber = streetNameAndNumber;
        }
        public StorageLocation(int storLocationId, DateTime removalDate)
        {
            StorLocationId = storLocationId;
            RemovalDate = removalDate;
         }
        #endregion Constructeurs
    }
}
