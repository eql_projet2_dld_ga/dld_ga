﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class CreditBundle
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public string Name { get; set; }
        public int CreditNumber { get; set; }
        public float Price { get; set; }
        #endregion
    }
}
