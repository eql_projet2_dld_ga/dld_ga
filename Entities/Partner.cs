﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Partner
    {
        #region Attributs
        public int PartnerId { get; set; }
        public int CityAndZipCodeId { get; set; }
        public string Appellation { get; set; }
        public string Email { get; set; }
        public string  Password { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int ValidationCode { get; set; }
        public DateTime EmailValidDate { get; set; }
        public DateTime AdmValidDate { get; set; }
        public string FirstName  { get; set; }
        public string LastName  { get; set; }
        public string Role  { get; set; }
        public DateTime BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DeregistrDate { get; set; }
        public string StreetNameAndNumber { get; set; }

        #endregion Attributs

        #region Constructeurs
        public Partner()
        {

        }
        public Partner(int partnerId,int cityAndZipCodeId, string appellation,
            string email, string password, DateTime registrationDate,
            int validationCode,DateTime emailValidDate,DateTime admValidDate, 
            string firstName, string lastName,
            string role, DateTime birthDate, string phoneNumber,
            DateTime deregistrDate, string streetNameAndNumber)
        {
            PartnerId = partnerId;
            CityAndZipCodeId = cityAndZipCodeId;
            Appellation = appellation;
            Email = email;
            Password = password;
            RegistrationDate = registrationDate;
            ValidationCode = validationCode;
            EmailValidDate = emailValidDate;
            AdmValidDate = admValidDate;
            FirstName = firstName;
            LastName = lastName;
            Role = role;
            BirthDate = birthDate;
            PhoneNumber = phoneNumber;
            DeregistrDate = deregistrDate;
            StreetNameAndNumber = streetNameAndNumber;
        }
        public Partner(int partnerId, int cityAndZipCodeId, string appellation,
                string email, string password, DateTime registrationDate,
                int validationCode, string firstName, string lastName,
                string role, DateTime birthDate, string phoneNumber,
                string streetNameAndNumber)
        {
            PartnerId = partnerId;
            CityAndZipCodeId = cityAndZipCodeId;
            Appellation = appellation;
            Email = email;
            Password = password;
            RegistrationDate = registrationDate;
            ValidationCode = validationCode;
            FirstName = firstName;
            LastName = lastName;
            Role = role;
            BirthDate = birthDate;
            PhoneNumber = phoneNumber;
            StreetNameAndNumber = streetNameAndNumber;
        }
        #endregion Constructeurs
    }
}
