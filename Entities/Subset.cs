﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Subset
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public int SubcategoryId { get; set; }
        public string Name { get; set; }
        public List<Product> Products { get; set; }
        #endregion
    }
}
