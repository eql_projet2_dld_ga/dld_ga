﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class StorageLocationDetails : StorageLocation
    {
        public StorageLocationDetails(StorageLocation sl)
        {
            StorLocationId = sl.StorLocationId;
            PartnerId= sl.PartnerId;
            CityAndZipCodeId = sl.CityAndZipCodeId;
            Name = sl.Name;
            PhoneNumber = sl.PhoneNumber;
            Picture = sl.Picture;
            AddDate= sl.AddDate;
            RemovalDate= sl.RemovalDate;
            StreetNameAndNumber = sl.StreetNameAndNumber;

        }
        public string PartnerName { get; set; }
        public string CityName { get; set; }
    }
}
