﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class Storage
    {
        #region Attributs
        public int StorId { get; set; }
        public int? PreservationMethId   { get; set; }
        public int? StorLocationId { get; set; }
        public int? StorageDimensionsId { get; set; }
        public int? StorageAccessibilityId   { get; set; }
        public string Name  { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime? RemovalDate { get; set; }
        #endregion Attributs

        #region Constructeurs
        public Storage()
        {

        }
        public Storage(int storId, int preservationMethId, int storLocationId,
            int storageDimensionsId, int storageAccessibilityId, string name,
            DateTime addDate, DateTime removalDate)
        {
            StorId = storId;
            PreservationMethId = preservationMethId;
            StorLocationId = storLocationId;
            StorageDimensionsId = storageDimensionsId;
            StorageAccessibilityId = storageAccessibilityId;
            Name = name;
            AddDate = addDate;
            RemovalDate = removalDate;
        }
        public Storage(int storId, int preservationMethId, int storLocationId,
              int storageDimensionsId, int storageAccessibilityId, string name,
               DateTime addDate)
        {
            StorId = storId;
            PreservationMethId = preservationMethId;
            StorLocationId = storLocationId;
            StorageDimensionsId = storageDimensionsId;
            StorageAccessibilityId = storageAccessibilityId;
            Name = name;
            AddDate = addDate;
        }
        #endregion Constructeurs
    }
}
