﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class CreditPurchase
    { 
        #region ATTRIBUTES
        public int PurchaseId { get; set; }
        public int BuyerId { get; set; }
        public int BundleId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int Quantity { get; set; }
        #endregion
    }
}
