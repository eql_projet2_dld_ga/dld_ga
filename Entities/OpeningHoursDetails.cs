﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class OpeningHoursDetails : OpeningHours
    {
        public string Weekday { get; set; }

        #region CONSTRUCTORS
        public OpeningHoursDetails()
        {

        }
        public OpeningHoursDetails(OpeningHours hours)
        {
            BusinessHoursId = hours.BusinessHoursId;
            WeekdayId = hours.WeekdayId;
            DonationId = hours.DonationId;
            StorLocationId = hours.StorLocationId;
            StorId = hours.StorId;
            OpeningHour = hours.OpeningHour;
            OpeningMinute = hours.OpeningMinute;
            ClosingHour = hours.ClosingHour;
            ClosingMinute = hours.ClosingMinute;
            BreakStartHour = hours.BreakStartHour;
            BreakStartMinute = hours.BreakStartMinute;
            BreakEndHour = hours.BreakEndHour;
            BreakEndMinute = hours.BreakEndMinute;
        }
        #endregion
    }
}
