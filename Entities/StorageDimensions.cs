﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Entities
{
    public class StorageDimensions
    {
        #region ATTRIBUTS
        public int Id { get; set; }
        public string CategorySize { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        #endregion

        #region CONSTRUCTEURS
        public StorageDimensions()
        {

        }
        public StorageDimensions(int id, string categorySize, int length, int width, int height)
        {
            Id = id;
            CategorySize = categorySize;
            Length = length;
            Width = width;
            Height = height;
        }
        #endregion
    }
}
