﻿using Fr.EQL.AI110.DLD_GA.Entities;
using Fr.EQL.AI110.DLD_GA.Business;
using Microsoft.AspNetCore.Mvc;
using Fr.EQL.AI110.DLD_GA.WebApp.Models;
namespace Fr.EQL.AI110.DLD_GA.WebApp.Controllers
{
    public class DonationController : Controller
    {
        #region SEARCH & BOOKING
        [HttpGet]
        public IActionResult Reservation(int id)
        {
            DonationSearchViewModel model = new DonationSearchViewModel();
            MemberBusiness mBU = new MemberBusiness();
            DonationBusiness dBu = new DonationBusiness();
            List<Member> members = mBU.SearchMembers();
            Donation don = dBu.GetDonationDetailsById(id);
            ViewBag.ListMembers = members;
            return View(don);
        }
        [HttpPost]
        public IActionResult Reservation(DonationDetails donDetails)
        {
            DonationBusiness dBu = new DonationBusiness();
            dBu.ReservedDonation(donDetails);
            return RedirectToAction("Booked");
        }
        public IActionResult Cancellation(DonationDetails donDetails)
        {
            DonationBusiness dBu = new DonationBusiness();
            dBu.CanceledDonation(donDetails);
            return RedirectToAction("Search");
        }

        [HttpGet]
        public IActionResult Booked()
        {
            DonationBusiness donBu = new DonationBusiness();
            MemberBusiness mBU = new MemberBusiness();
            ProductOriginBusiness poBu = new ProductOriginBusiness();
            CategoryBusiness cBU = new CategoryBusiness();
            CityAndZipCodeBusiness cityBU = new CityAndZipCodeBusiness();

            DonationSearchViewModel model = new DonationSearchViewModel();

            model.Cities = cityBU.GetCities();
            model.Members = mBU.SearchMembers();
            model.Origins = poBu.SearchOrigins();
            model.Categories = cBU.GetCategoriesInDepth();
            model.Donations = donBu.GetBookedDonations(0);

            bool isRegistered = false;
            model.AllDonations = new List<DonationDetails>();

            List<DonationDetails> list = donBu.GetBookedDonations(0);
            foreach (DonationDetails donDupli in list)
            {
                if (model.AllDonations != null)
                {
                    foreach (DonationDetails donFinal in model.AllDonations)
                    {
                        if (donFinal.RecipientId == donDupli.RecipientId)
                        {
                            isRegistered = true;
                            break;
                        }
                    }
                }
                if (isRegistered == false)
                {
                    model.AllDonations.Add(donDupli);
                }
                else
                {
                    isRegistered = false;
                }
            }
            return View(model);

        }
        [HttpPost]
        public IActionResult Booked(DonationSearchViewModel model)
        {
            DonationBusiness donBu = new DonationBusiness();
            MemberBusiness mBU = new MemberBusiness();
            ProductOriginBusiness poBu = new ProductOriginBusiness();
            CategoryBusiness cBU = new CategoryBusiness();
            CityAndZipCodeBusiness cityBU = new CityAndZipCodeBusiness();
            model.Cities = cityBU.GetCities();
            model.Members = mBU.SearchMembers();
            model.Origins = poBu.SearchOrigins();
            model.Categories = cBU.GetCategoriesInDepth();
            model.Donations = donBu.GetBookedDonations(model.MemberId);

            bool isRegistered = false;
            model.AllDonations = new List<DonationDetails>();

            List<DonationDetails> list = donBu.GetBookedDonations(0);
            foreach (DonationDetails donDupli in list)
            {
                if (model.AllDonations != null)
                {
                    foreach (DonationDetails donFinal in model.AllDonations)
                    {
                        if (donFinal.RecipientId == donDupli.RecipientId)
                        {
                            isRegistered = true;
                            break;
                        }
                    }
                }
                if (isRegistered == false)
                {
                    model.AllDonations.Add(donDupli);
                }
                else
                {
                    isRegistered = false;
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Search()
        {
            DonationBusiness donBu = new DonationBusiness();
            MemberBusiness mBU = new MemberBusiness();
            ProductOriginBusiness poBu = new ProductOriginBusiness();
            CategoryBusiness cBU = new CategoryBusiness();
            CityAndZipCodeBusiness cityBU = new CityAndZipCodeBusiness();

            DonationSearchViewModel model = new DonationSearchViewModel();

            model.Cities = cityBU.GetCities();
            model.Members = mBU.SearchMembers();
            model.Origins = poBu.SearchOrigins();
            model.Categories = cBU.GetCategoriesInDepth();
            model.Donations = donBu.GetDonations(0, "", 0, 0, 0);
            return View(model);
        }


        [HttpPost]
        public IActionResult Search(DonationSearchViewModel model)
        {
            DonationBusiness donBu = new DonationBusiness();
            MemberBusiness mBU = new MemberBusiness();
            ProductOriginBusiness poBu = new ProductOriginBusiness();
            CategoryBusiness cBU = new CategoryBusiness();
            CityAndZipCodeBusiness cityBU = new CityAndZipCodeBusiness();
            model.Cities = cityBU.GetCities();
            model.Members = mBU.SearchMembers();
            model.Origins = poBu.SearchOrigins();
            model.Categories = cBU.GetCategoriesInDepth();
            model.Donations = donBu.GetDonations(model.ProductOriginId, model.City, model.ZipCode, model.StorageId, model.ProductId);
            return View(model);
        }
        #endregion

        #region SUBMIT
        [HttpGet]
        public IActionResult Create()
        {
            MemberBusiness mBU = new MemberBusiness();
            ProductOriginBusiness poBu = new ProductOriginBusiness();
            CategoryBusiness catBU = new CategoryBusiness();

            DonationCreationViewModel model = new DonationCreationViewModel();
            model.Don = new Donation();
            model.Members = mBU.SearchMembers();
            model.Origins = poBu.SearchOrigins();
            model.Categories = catBU.GetCategoriesInDepth();

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(DonationCreationViewModel previousModel)
        {
            CityAndZipCodeBusiness cityBU = new CityAndZipCodeBusiness();
            MemberBusiness mBU = new MemberBusiness();
            ProductBusiness poBu = new ProductBusiness();
            UnitBusiness unitBU = new UnitBusiness();
            StorageBusiness stoBU = new StorageBusiness();

            DonationPlaceSelectionViewModel newModel = new DonationPlaceSelectionViewModel();
            newModel.Cities = cityBU.GetCities();
            newModel.Don = previousModel.Don;
            newModel.Donor = mBU.GetMemberDetailsById(previousModel.Don.DonorId);

            Product product = poBu.GetproductById(previousModel.Don.ProductId);
            newModel.ProductName = product.Name;

            Unit unit = unitBU.GetUnitById(previousModel.Don.UnitId);
            newModel.UnitName = unit.Name;

            newModel.AvailableStoragesForProduct = stoBU.GetAvailableStoragesByPreservationMethod(product.PreservationId);

            return View("PlaceSelection", newModel);

        }

        [HttpPost]
        public IActionResult OpeningHours(DonationPlaceSelectionViewModel oldModel)
        {
            try
            {
                WeekdayBusiness wBU = new WeekdayBusiness();
                OpeningHoursBusiness hoursBU = new OpeningHoursBusiness();
                DonationBusiness donBU = new DonationBusiness();
                int donId = donBU.SaveDonationId(oldModel.Don);


                DonationOpeningHoursCreationViewModel newModel = new DonationOpeningHoursCreationViewModel();
                newModel.Weekdays = wBU.GetWeekdays();
                newModel.Don = donBU.GetDonationDetailsById(donId);
                newModel.AllOpeningHours = hoursBU.GetAllHoursDetailsByDonationId(donId);

                return View(newModel);
            }
            catch (Exception ex)
            {
                MemberBusiness mBU = new MemberBusiness();
                ProductOriginBusiness poBu = new ProductOriginBusiness();
                CategoryBusiness catBU = new CategoryBusiness();

                DonationCreationViewModel model = new DonationCreationViewModel();
                model.Don = new Donation();
                model.Don.Quantity = oldModel.Don.Quantity;
                model.Members = mBU.SearchMembers();
                model.Origins = poBu.SearchOrigins();
                model.Categories = catBU.GetCategoriesInDepth();

                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Create", model);
            }
            
        }
        [HttpPost]
        public IActionResult LoopOpeningHours(DonationOpeningHoursCreationViewModel model)
        {
            WeekdayBusiness wBU = new WeekdayBusiness();
            OpeningHoursBusiness hoursBU = new OpeningHoursBusiness();
            DonationBusiness donBU = new DonationBusiness();

            model.ThisDayOpeningHours.DonationId = model.Don.Id;
            hoursBU.SaveOpeningHours(model.ThisDayOpeningHours);

            model.Don = donBU.GetDonationDetailsById(model.Don.Id);
            model.AllOpeningHours = hoursBU.GetAllHoursDetailsByDonationId(model.Don.Id);
            model.ThisDayOpeningHours = null;

            List<Weekday> allWeekdays = wBU.GetWeekdays();
            model.Weekdays = new List<Weekday>();
            bool isRegistered = false;
            foreach (Weekday day in allWeekdays)
            {
                foreach (OpeningHours registeredDay in model.AllOpeningHours)
                {
                    if (day.Id == registeredDay.WeekdayId)
                    {
                        isRegistered = true;
                        break;
                    }
                }
                if (isRegistered == false)
                {
                    model.Weekdays.Add(day);
                }
                else
                {
                    isRegistered = false;
                }
            }

            return View("OpeningHours", model);
        }

        [HttpPost]
        public async Task<IActionResult> RegisterPicture(DonationOpeningHoursCreationViewModel model)
        {
            IFormFile file = model.DonationPicture;
            string pictureName = model.Don.Id + ".png";

            if (file != null && file.Length != 0)
            {
                var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot\\images\\donations",
                            pictureName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                DonationBusiness donBU = new DonationBusiness();
                donBU.SaveDonationPicture(pictureName, model.Don.Id);
            }
            return RedirectToAction("Search");
        }

        [HttpPost]
        public async Task<IActionResult> DonationRegister(DonationPlaceSelectionViewModel model)
        {
            try { 
            Donation don = model.Don;

            DonationBusiness donBU = new DonationBusiness();
            model.Don.Id = donBU.SaveDonationId(don);

            IFormFile file = model.DonationPicture;
            string pictureName = model.Don.Id + ".png";

            if (file != null && file.Length != 0)
            {
                var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot\\images\\donations",
                            pictureName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                donBU.SaveDonationPicture(pictureName, model.Don.Id);
            }
                return RedirectToAction("Search");
            }
            catch (Exception ex)
            {
                MemberBusiness mBU = new MemberBusiness();
                ProductOriginBusiness poBu = new ProductOriginBusiness();
                CategoryBusiness catBU = new CategoryBusiness();

                DonationCreationViewModel newModel = new DonationCreationViewModel();
                newModel.Don = new Donation();
                newModel.Don.Quantity = model.Don.Quantity;
                newModel.Members = mBU.SearchMembers();
                newModel.Origins = poBu.SearchOrigins();
                newModel.Categories = catBU.GetCategoriesInDepth();

                TempData["ErrorMessage"] = ex.Message;
                return RedirectToAction("Create", newModel);
            }
        }
        #endregion

    }
}
