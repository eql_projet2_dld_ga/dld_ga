﻿using Fr.EQL.AI110.DLD_GA.Business;
using Fr.EQL.AI110.DLD_GA.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.DLD_GA.WebApp.Controllers
{
    public class CreditPurchaseController : Controller
    {
        public IActionResult Index()
        {
            CreditPurchaseBusiness cBU = new CreditPurchaseBusiness();
            List<CreditPurchaseDetails> purchases = cBU.GetAllPurchaseDetails();

            return View(purchases);
        }
    }
}
