﻿using Fr.EQL.AI110.DLD_GA.Business;
using Fr.EQL.AI110.DLD_GA.Entities;
using Fr.EQL.AI110.DLD_GA.WebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.DLD_GA.WebApp.Controllers
{
    public class StorageController : Controller
    {
        private IActionResult CreateStorage(Storage st)
        {
            StorageLocationBusiness slbu = new StorageLocationBusiness();
            PreservationMethodBusiness pmbu = new PreservationMethodBusiness();
            StorageDimensionsBusiness sdbu = new StorageDimensionsBusiness();
            StorageAccessibilityBusiness sabu = new StorageAccessibilityBusiness();

            List<PreservationMethod> methods = pmbu.GetPreservationMethod();
            List<StorageDimensions> dimensions = sdbu.GetStorageDimensions();
            List<StorageLocation> locations = slbu.GetStorageLocations();
            List<StorageAccessibility> acces = sabu.GetStorageAccessibility();

            ViewBag.ListeLocations = locations;
            ViewBag.ListeMethods = methods;
            ViewBag.ListeDimensions = dimensions;
            ViewBag.ListeAccessibilities = acces;

            return View("Create", st);
        }
        private IActionResult UpdateStorage(Storage st)
        {
            StorageBusiness bu = new StorageBusiness();
            StorageLocationBusiness slbu = new StorageLocationBusiness();
            PreservationMethodBusiness pmbu = new PreservationMethodBusiness();
            StorageDimensionsBusiness sdbu = new StorageDimensionsBusiness();
            StorageAccessibilityBusiness sabu = new StorageAccessibilityBusiness();

            List<Storage> storages = bu.GetStorages();
            List<PreservationMethod> methods = pmbu.GetPreservationMethod();
            List<StorageDimensions> dimensions = sdbu.GetStorageDimensions();
            List<StorageLocation> locations = slbu.GetStorageLocations();
            List<StorageAccessibility> acces = sabu.GetStorageAccessibility();

            ViewBag.ListeStorages = storages;
            ViewBag.ListeLocations = locations;
            ViewBag.ListeMethods = methods;
            ViewBag.ListeDimensions = dimensions;
            ViewBag.ListeAccessibilities = acces;

            return View("Update", st);
        }
        private IActionResult DeleteStorage(Storage st)
        {
            StorageBusiness bu = new StorageBusiness();
            List<Storage> storages = bu.GetStorages();
            ViewBag.ListeStorages = storages;

            return View("Delete", st);
        }
        public IActionResult Index()
        {
            StorageBusiness bu = new StorageBusiness();
            List<Storage> sts = bu.GetStorages();
            return View(sts);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return CreateStorage(null);
        }
        [HttpGet]
        public IActionResult Update()
        {
            return UpdateStorage(null);
        }
        [HttpGet]
        public IActionResult Delete()
        {
            return DeleteStorage(null);
        }
        [HttpGet]
        public IActionResult Edit()
        {
        
                StorageLocationBusiness slbu = new StorageLocationBusiness();
                PreservationMethodBusiness pmbu = new PreservationMethodBusiness();
                StorageDimensionsBusiness sdbu = new StorageDimensionsBusiness();
                StorageAccessibilityBusiness sabu = new StorageAccessibilityBusiness();
                StorageBusiness bu = new StorageBusiness();
                StorageViewModel model = new StorageViewModel();

                model.Locations = slbu.GetStorageLocations();
                model.Methods = pmbu.GetPreservationMethod();
                model.Dimensions = sdbu.GetStorageDimensions();
                model.Accessibilities = sabu.GetStorageAccessibility();
                model.Details = bu.GetStorageInfos(0);
                return View(model);
        }
        [HttpPost]
        public IActionResult Create(Storage st)
        {
            StorageBusiness bu = new StorageBusiness();
            bu.SaveStorage(st);
            return RedirectToAction("Edit");
        }
        [HttpPost]
        public IActionResult Delete(Storage st)
        {
            StorageBusiness bu = new StorageBusiness();
            bu.DesactiveStorage(st);
            return RedirectToAction("Edit");
        }
        [HttpPost]
        public IActionResult Update(Storage st)
        {
            StorageBusiness bu = new StorageBusiness();
            bu.UpdateStorage(st);
            return RedirectToAction("Edit");
        }
        [HttpPost]
        public IActionResult Edit(StorageViewModel model)
        {
            StorageLocationBusiness slbu = new StorageLocationBusiness();
            PreservationMethodBusiness pmbu = new PreservationMethodBusiness();
            StorageDimensionsBusiness sdbu = new StorageDimensionsBusiness();
            StorageAccessibilityBusiness sabu = new StorageAccessibilityBusiness();
            StorageBusiness bu = new StorageBusiness();

            model.Locations = slbu.GetStorageLocations();
            model.Methods= pmbu.GetPreservationMethod();
            model.Dimensions= sdbu.GetStorageDimensions();
            model.Accessibilities = sabu.GetStorageAccessibility();
            model.Details = bu.GetStorageInfos(model.StorLocationId);
            return View(model);
        }
    }
}
