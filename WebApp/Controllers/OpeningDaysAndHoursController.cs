﻿using Fr.EQL.AI110.DLD_GA.Business;
using Fr.EQL.AI110.DLD_GA.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.DLD_GA.WebApp.Controllers
{
    public class OpeningDaysAndHoursController : Controller
    {
        private IActionResult EditerOpeningTime(OpeningHours openTime)
        {
            StorageLocationBusiness slBu = new StorageLocationBusiness();
            List<StorageLocation> sls = slBu.GetStorageLocations();
            ViewBag.ListeStorageLocations = sls;
            StorageBusiness storageBu = new StorageBusiness();
            List<Storage> storages = storageBu.GetStorages();
            ViewBag.ListeStorages = storages;
            WeekdayBusiness wdayBu = new WeekdayBusiness();
            List<Weekday> wDays = wdayBu.GetWeekdays();
            ViewBag.ListeWeekdays = wDays;

            return View("Create", openTime);
        }

        #region HttpGet
        [HttpGet]
        public IActionResult Create()
        {
            return EditerOpeningTime(null);
        }
        #endregion HttpGet

        #region HttpPost
        [HttpPost]
        public IActionResult Create(OpeningHours openTime)
        {
            Weekday wday= new Weekday();
            OpeningHoursBusiness openTimeBU = new OpeningHoursBusiness();
            openTimeBU.SaveOpeningHours(openTime);
            return RedirectToAction("Create");
        }
        #endregion 

    }
}
