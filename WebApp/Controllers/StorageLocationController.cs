﻿using Fr.EQL.AI110.DLD_GA.Business;
using Fr.EQL.AI110.DLD_GA.Entities;
using Fr.EQL.AI110.DLD_GA.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Models;


namespace Fr.EQL.AI110.DLD_GA.WebApp.Controllers
{
    public class StorageLocationController : Controller
    {
        private IActionResult CreateStorageLocation(StorageLocation sl)
        {
            PartnerBusiness partnerBu = new PartnerBusiness();
            CityAndZipCodeBusiness cityBu=new CityAndZipCodeBusiness();
            List<Partner> partners = partnerBu.GetPartners();
            List<CityAndZipCode> cities = cityBu.GetCities();
            
            ViewBag.ListePartners = partners;
            ViewBag.ListeCities = cities;

            return View("Create", sl);
        }
        private IActionResult EditorStorageLocation(StorageLocation sl)
        {
            PartnerBusiness partnerBu = new PartnerBusiness();
            CityAndZipCodeBusiness cityBu = new CityAndZipCodeBusiness();
            StorageLocationBusiness slBu = new StorageLocationBusiness();
            List<StorageLocation> sls = slBu.GetStorageLocations();
            List<Partner> partners = partnerBu.GetPartners();
            List<CityAndZipCode> cities = cityBu.GetCities();

            ViewBag.ListePartners = partners;
            ViewBag.ListeCities = cities;
            ViewBag.ListeSls = sls;

            return View("Edit", sl);
        }
        private IActionResult UpdateStorageLocation(StorageLocation sl)
        {
            PartnerBusiness partnerBu = new PartnerBusiness();
            CityAndZipCodeBusiness cityBu = new CityAndZipCodeBusiness();
            StorageLocationBusiness slBu = new StorageLocationBusiness();
            List<StorageLocation> sls = slBu.GetStorageLocations();
            List<Partner> partners = partnerBu.GetPartners();
            List<CityAndZipCode> cities = cityBu.GetCities();

            ViewBag.ListePartners = partners;
            ViewBag.ListeCities = cities;
            ViewBag.ListeSls = sls;

            return View("Update", sl);
        }
        private IActionResult DeleteStorageLocation(StorageLocation sl)
        {
            StorageLocationBusiness slBu = new StorageLocationBusiness();
            List<StorageLocation> sls = slBu.GetStorageLocations();
            ViewBag.ListeSls = sls;

            return View("Delete", sl);
        }
        public IActionResult Index()
        {
            StorageLocationBusiness slBu = new StorageLocationBusiness();
            List<StorageLocation> sls = slBu.GetStorageLocations();
            return View(sls);
        }

        #region HttpGet
        [HttpGet]
        public IActionResult Create()
        { 
            return CreateStorageLocation(null);
        }
        [HttpGet]
        public IActionResult Update()
        {
            return UpdateStorageLocation(null);
        }
        [HttpGet]
        public IActionResult Delete()
        {
            return DeleteStorageLocation(null);
        }
        [HttpGet]
        public IActionResult Edit()
        {
            PartnerBusiness partnerBu = new PartnerBusiness();
            CityAndZipCodeBusiness cityBu = new CityAndZipCodeBusiness();
            StorageLocationBusiness slBu = new StorageLocationBusiness();

            StorageLocationViewModel model = new StorageLocationViewModel();
            model.Partners = partnerBu.GetPartners();
            model.CityAndZipCodes = cityBu.GetCities();
            model.Details = slBu.GetStorageLocationInfos(0);
            return View(model);
        }
        #endregion HttpGet

        #region HttpPost
        [HttpPost]
        public IActionResult Delete(StorageLocation sl)
        {
            StorageLocationBusiness slBu = new StorageLocationBusiness();
            slBu.DesactiveStorageLocation(sl);
            return RedirectToAction("Edit");
        }
        [HttpPost]
        public IActionResult Update(StorageLocation sl)
        {
            StorageLocationBusiness slBu = new StorageLocationBusiness();
            slBu.UpdateStorageLocation(sl);
            return RedirectToAction("Edit");
        }
        [HttpPost]
        public IActionResult Create(StorageLocation sl)
        {
            StorageLocationBusiness storageLocationBU = new StorageLocationBusiness();
            storageLocationBU.SaveStorageLocation(sl);
            return RedirectToAction("Edit");
        }
        [HttpPost]
        public IActionResult Edit(StorageLocationViewModel model)
        {
            PartnerBusiness partnerBu = new PartnerBusiness();
            CityAndZipCodeBusiness cityBu = new CityAndZipCodeBusiness();
            StorageLocationBusiness slBu = new StorageLocationBusiness();

            model.Partners = partnerBu.GetPartners();
            model.CityAndZipCodes = cityBu.GetCities();
            model.Details = slBu.GetStorageLocationInfos(model.PartnerId);
            return View(model);
        }
        #endregion HttpPost

        #region Error
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion Error
    }
}
