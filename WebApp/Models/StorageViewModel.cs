﻿using Fr.EQL.AI110.DLD_GA.Entities;
using Fr.EQL.AI110.DLD_GA.WebApp.Models;


namespace Fr.EQL.AI110.DLD_GA.WebApp.Models
{
    public class StorageViewModel
    {
        public int StorLocationId { get; set; }
        public int StorId { get; set; }
        public int PreservationMethId { get; set; }
        public int StorageDimensionsId { get; set; }
        public int StorageAccessibilityId { get; set; }
        public string Name { get; set; }

        #region Data to display
        public List<StorageLocation> Locations { get; set; }
        public List<Storage> Storages { get; set; }
        public List<StorageDetails> Details { get; set; }
        public List<PreservationMethod> Methods { get; set; }
        public List<StorageDimensions> Dimensions { get; set; }
        public List<StorageAccessibility> Accessibilities { get; set; }
        #endregion

    }
}