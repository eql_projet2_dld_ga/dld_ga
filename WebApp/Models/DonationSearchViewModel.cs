﻿using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.WebApp.Models
{
    public class DonationSearchViewModel
    {
        #region Search Criteria
        public int ProductId { get; set; }
        public int ProductOriginId { get; set; }
        public string City { get; set; }
        public int ZipCode { get; set; }
        public int StorageId { get; set; }
        public int MemberId { get; set; }
        #endregion


        #region Data list to display
        public DonationDetails DonDetails { get; set; }
        public List<DonationDetails> AllDonations { get; set; }
        public List<DonationDetails> Donations { get; set; }
        public List<Member> Members { get; set; }
        public List<ProductOrigin> Origins { get; set; }
        public List<Category> Categories { get; set; }
        public List<CityAndZipCode> Cities { get; set;}
        #endregion
    }
}
