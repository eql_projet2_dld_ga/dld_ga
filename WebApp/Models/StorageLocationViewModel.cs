﻿using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.WebApp.Models
{
    public class StorageLocationViewModel
    {
        public int StorLocationId { get; set; }
        public int PartnerId { get; set; }
        public int CityAndZipCodeId { get; set; }

        #region Data to display
        public List<Partner> Partners { get; set; }
        public List<StorageLocation> StorageLocations { get; set; }
        public List<StorageLocationDetails> Details { get; set; }
        public List<CityAndZipCode> CityAndZipCodes { get; set;}
        #endregion
    }
}
