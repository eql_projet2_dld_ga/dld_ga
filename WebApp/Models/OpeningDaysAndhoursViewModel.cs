﻿using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.WebApp.Models
{
    public class OpeningDaysAndhoursViewModel
    {
        public List<Partner> Partners { get; set; }
        public List<Weekday> Weekdays { get; set; }
        public List<CityAndZipCode> CityAndZipCode { get; set;}

    }
}
