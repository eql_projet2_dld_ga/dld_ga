﻿using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.WebApp.Models
{
    public class DonationOpeningHoursCreationViewModel
    {
        public DonationDetails Don { get; set; }
        public IFormFile DonationPicture { get; set; }
        public List<Weekday> Weekdays { get; set; }
        public List<OpeningHoursDetails> AllOpeningHours { get; set; }
        public OpeningHours ThisDayOpeningHours { get; set; }
    }
}
