﻿using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.WebApp.Models
{
    public class DonationCreationViewModel
    {
        #region ATTRIBUTES
        public Donation Don { get; set; }
        public List<Member> Members { get; set; }
        public List<ProductOrigin> Origins { get; set; }
        public List<Category> Categories { get; set; }
        public IFormFile DonationPicture { get; set; }
        
        #endregion

    }
}
