﻿using Fr.EQL.AI110.DLD_GA.Entities;
namespace Fr.EQL.AI110.DLD_GA.WebApp.Models
{
    public class DonationPlaceSelectionViewModel
    {
        public Donation Don { get; set; }
        public string ProductName { get; set; }
        public string UnitName { get; set; }
        public IFormFile DonationPicture { get; set; }
        public MemberDetails Donor { get; set; }
        public List<CityAndZipCode> Cities { get; set; }
        public List<StorageDetails> AvailableStoragesForProduct { get; set; }

    }
}
