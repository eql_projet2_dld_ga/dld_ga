﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class StorageLocationBusiness
    {
        public void DesactiveStorageLocation(StorageLocation storageLocation)
        {
            StorageLocationDAO slDAO = new StorageLocationDAO();
            storageLocation.RemovalDate = DateTime.Now;
            slDAO.Desactive(storageLocation);
        }
        public void UpdateStorageLocation(StorageLocation storageLocation)
        {
            StorageLocationDAO slDAO = new StorageLocationDAO();
            storageLocation.AddDate = DateTime.Now;
            slDAO.Update(storageLocation);
        }
        public void SaveStorageLocation(StorageLocation storageLocation)
        {
            StorageLocationDAO slDAO = new StorageLocationDAO();
            storageLocation.AddDate=DateTime.Now;
            slDAO.Insert(storageLocation);

        }
        public List<StorageLocation> GetStorageLocations()
        {
            StorageLocationDAO dao = new StorageLocationDAO();
            return dao.GetAll();
        }
        public List<StorageLocationDetails> GetStorageLocationInfos(int PartnerId)
        {
            StorageLocationDAO sldao = new StorageLocationDAO();
            return sldao.GetByMultiCriteria(PartnerId);
        }
        public StorageLocation GetStorageLocationById(int storLocationId)
        {
            return new StorageLocationDAO().GetById(storLocationId);
        }

    }
}
