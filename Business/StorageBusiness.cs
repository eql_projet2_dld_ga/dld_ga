﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class StorageBusiness
    {
        public List<StorageDetails> GetAvailableStoragesByPreservationMethod(int id)
        {
            StorageDAO dao = new StorageDAO();
            List<StorageDetails> results = dao.GetAllAvailableByPreservationMethod(id);
            return results;
        }
        public void DesactiveStorage(Storage st)
        {
            StorageDAO dao = new StorageDAO();
            st.RemovalDate = DateTime.Now;
            dao.Desactive(st);
        }
        public void UpdateStorage(Storage st)
        {
            StorageDAO dao = new StorageDAO();
            st.AddDate = DateTime.Now;
            dao.Update(st);
        }
        public void SaveStorage(Storage st)
        {
            StorageDAO dao = new StorageDAO();
            st.AddDate = DateTime.Now;
            dao.Insert(st);
        }
        public List<Storage> GetStorages()
        {
            StorageDAO dao = new StorageDAO();
            return dao.GetAll();
        }
        public List<StorageDetails> GetStorageInfos(int StorLocationId)
        {
            StorageDAO dao = new StorageDAO();
            return dao.GetByMultiCriteria(StorLocationId);
        }
        public Storage GetStorageById(int storId)
        {
            return new StorageDAO().GetById(storId);
        }

    }
}
