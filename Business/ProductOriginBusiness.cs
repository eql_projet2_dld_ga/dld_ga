﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class ProductOriginBusiness
    {
        public List<ProductOrigin> SearchOrigins()
        {
            ProductOriginDAO dao = new ProductOriginDAO();
            return dao.GetAll();
        }
    }
}
