﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class MemberBusiness
    {
        public List<Member> SearchMembers()
        {
            MemberDAO dao = new MemberDAO();
            return dao.GetAll();
        }

        public MemberDetails GetMemberDetailsById(int id)
        {
            MemberDAO dao = new MemberDAO();
            return dao.GetDetailsById(id);
        }
    }
}
