﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class OpeningHoursBusiness
    {
        public void SaveOpeningHours(OpeningHours openingHours)
        {
            OpeningHoursDAO openingHoursDAO = new OpeningHoursDAO();

            openingHoursDAO.Insert(openingHours);
        }
        public void SaveWeekend(OpeningHours openWeekend)
        {
            OpeningHoursDAO dao = new OpeningHoursDAO();
            OpeningHours saturday = openWeekend;
            saturday.WeekdayId = 6;
            dao.Insert(saturday);
            OpeningHours sunday = openWeekend;
            sunday.WeekdayId = 7;
            dao.Insert(sunday);
        }
        public void SaveMondayToFriday(OpeningHours openMondayToFriday)
        {
            OpeningHoursDAO dao = new OpeningHoursDAO();
            OpeningHours monday = openMondayToFriday;
            monday.WeekdayId = 1;
            dao.Insert(monday);
            OpeningHours tuesday = openMondayToFriday;
            tuesday.WeekdayId = 2;
            dao.Insert(tuesday);
            OpeningHours wednesday= openMondayToFriday;
            wednesday.WeekdayId= 3;
            dao.Insert(wednesday);
            OpeningHours thursday= openMondayToFriday;
            thursday.WeekdayId= 4;
            dao.Insert(thursday);
            OpeningHours friday= openMondayToFriday;
            friday.WeekdayId= 5;
            dao.Insert(friday);
        }

        public List<OpeningHoursDetails> GetAllHoursDetailsByDonationId(int id)
        {
            OpeningHoursDAO dao = new OpeningHoursDAO();
            return dao.GetAllDetailsByDonationId(id);
        }
    }
}
