﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class StorageDimensionsBusiness
    {
        public List<StorageDimensions> GetStorageDimensions()
        {
            StorageDimensionsDAO dao = new StorageDimensionsDAO();
            return dao.GetAll();
        }
    }
}
