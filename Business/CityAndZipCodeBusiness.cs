﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class CityAndZipCodeBusiness
    {
        public void SaveCityAndZipCode(CityAndZipCode cityAndZipCode)
        {
            CityAndZipCodeDAO cityAndZipCodeDAO = new CityAndZipCodeDAO();
            cityAndZipCodeDAO.Insert(cityAndZipCode);
        }
        public List<CityAndZipCode> GetCities()
        {
            CityAndZipCodeDAO dao = new CityAndZipCodeDAO();
            return dao.GetAll();
        }
    }
}
