﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class CategoryBusiness
    {
        public List<Category> GetCategories()
        {
            CategoryDAO dao = new CategoryDAO();
            return dao.GetAll();
        }

        public List<Category> GetCategoriesInDepth()
        {
            SubCategoryBusiness subcatBU = new SubCategoryBusiness();
            SubsetBusiness setBU = new SubsetBusiness();
            ProductBusiness pBU = new ProductBusiness();
            UnitBusiness uBU = new UnitBusiness();

            List<Category> results = GetCategories();

            foreach (Category cat in results)
            {
                cat.SubCategories = subcatBU.SearchSubCategoryByCategoryId(cat.Id);

                foreach (SubCategory subcat in cat.SubCategories)
                {
                    subcat.Subsets = setBU.SearchSubsetBySubCategoryId(subcat.Id);

                    foreach (Subset set in subcat.Subsets)
                    {
                        set.Products = pBU.SearchProductsBySubsetId(set.Id);

                        foreach (Product product in set.Products)
                        {
                            product.Units = uBU.SearchUnitsByProductId(product.Id);

                        }
                    }
                }
            }
            return results;
        }
    }
}
