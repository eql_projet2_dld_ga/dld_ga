﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class ProductBusiness
    {
        public Product GetproductById(int id)
        {
            ProductDAO dao = new ProductDAO();
            return dao.GetById(id);
        }
        public List<Product> SearchProductsBySubsetId(int id)
        {
            ProductDAO dao = new ProductDAO();
            return dao.GetBySubsetId(id);
        }
    }
}
