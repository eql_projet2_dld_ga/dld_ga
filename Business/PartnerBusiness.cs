﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class PartnerBusiness
    {
        public void SavePartner(Partner partner)
        {
            PartnerDAO partnerDAO = new PartnerDAO();
            partnerDAO.Insert(partner); 
        }
        public List<Partner> GetPartners()
        {
            PartnerDAO dao = new PartnerDAO();
            return dao.GetAll();
        }
    }
}
