﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GA.Entities;
using Fr.EQL.AI110.DLD_GA.DataAccess;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class DonationBusiness
    {
        public DonationDetails GetDonationDetailsById(int id)
        {
            DonationDAO donDAO = new DonationDAO();
            return donDAO.GetDetailsById(id);
        }

        public List<DonationDetails> GetDonations(int ProductOriginId, string City, int ZipCode, int StorageId, int ProductId)
        {
            DonationDAO dao = new DonationDAO();
            return dao.GetByMultiCriteria(ProductOriginId, City, ZipCode, StorageId, ProductId);
        }
        public List<DonationDetails> GetBookedDonations(int MemberId)
        {
            DonationDAO dao = new DonationDAO();
            return dao.GetByRecipient(MemberId);
        }

        public void SaveDonation(Donation don)
        {
            if (don.ProductId <= 0 || don.UnitId <=0)
            {
                throw new Exception("Il faut choisir un produit et une unité de mesure.");
            }
            if (don.ExpirationDate <= DateTime.Now)
            {
                throw new Exception("La date de péremption est obligatoire et ne doit pas être passée.");
            }
            if (don.Quantity <= 0)
            {
                throw new Exception("Il est interdit de proposer une quantité nulle ou négative");
            }
            

            DonationDAO dao = new DonationDAO();
            dao.Insert(don);
        }

        public void CanceledDonation(DonationDetails donDetails)
        {
            DonationDAO dao = new DonationDAO();
            dao.CancelDonation(donDetails);
        }

        public void ReservedDonation(DonationDetails donDetails)
        {
            DonationDAO dao = new DonationDAO();
            dao.BookDonation(donDetails);
        }

        public int SaveDonationId(Donation don)
        {
            if (don.ProductId <= 0 || don.UnitId <= 0)
            {
                throw new Exception("Il faut choisir un produit et une unité de mesure.");
            }
            if (don.ExpirationDate <= DateTime.Now)
            {
                throw new Exception("La date de péremption est obligatoire et ne doit pas être passée.");
            }
            if (don.Quantity <= 0)
            {
                throw new Exception("Il est interdit de proposer une quantité nulle ou négative");
            }


            DonationDAO dao = new DonationDAO();
            return dao.InsertId(don);
        }

        public void SaveDonationPicture(string pictureName, int id)
        {
            DonationDAO dao = new DonationDAO();
            dao.UpdatePicture(pictureName, id);

        }
    }
}
