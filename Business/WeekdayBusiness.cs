﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class WeekdayBusiness
    {
        public void SaveWeekday(Weekday wday)
        {
            WeekdayDAO wdayDAO = new WeekdayDAO();
            wdayDAO.Insert(wday);
        }
        public List<Weekday> GetWeekdays()
        {
            WeekdayDAO dao = new WeekdayDAO();
            return dao.GetAll();
        }
    }
}
