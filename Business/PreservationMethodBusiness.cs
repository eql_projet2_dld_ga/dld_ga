﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class PreservationMethodBusiness
    {

        public List<PreservationMethod> GetPreservationMethod()
        {
            PreservationMethodDAO dao = new PreservationMethodDAO();
            return dao.GetAll();
        }
        public PreservationMethod GetPreservationMethodById(int id)
        {
            PreservationMethodDAO dao = new PreservationMethodDAO();
            return dao.GetById(id);

        }
    }
}
