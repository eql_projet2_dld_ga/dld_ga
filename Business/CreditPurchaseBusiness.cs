﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class CreditPurchaseBusiness
    {
        public List<CreditPurchaseDetails> GetAllPurchaseDetails()
        {
            CreditPurchaseDAO dao = new CreditPurchaseDAO();
            return dao.GetAllDetails();
        }
    }
}
