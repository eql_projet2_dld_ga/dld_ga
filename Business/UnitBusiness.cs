﻿using Fr.EQL.AI110.DLD_GA.DataAccess;
using Fr.EQL.AI110.DLD_GA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GA.Business
{
    public class UnitBusiness
    {
        public Unit GetUnitById(int id)
        {
            UnitDAO dao = new UnitDAO();
            return dao.GetById(id);
        }
        public List<Unit> SearchUnitsByProductId(int id)
        {
            UnitDAO dao = new UnitDAO();
            return dao.GetByProductId(id);
        }
            
    }
}
